using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_POPUPMODULE_V0_1
{
    public class UserModuleClass_IESS_POPUPMODULE_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> POPUPINPUT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> POPUPOUTPUT;
        ushort [] GVPOPUPQUEUE;
        private ushort CHECKQUEUEINPUTS (  SplusExecutionContext __context__, ushort LVINPUT ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVACTIVE = 0;
            
            
            __context__.SourceCodeLine = 70;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)16; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 72;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINPUT == GVPOPUPQUEUE[ LVCOUNTER ]))  ) ) 
                    {
                    __context__.SourceCodeLine = 73;
                    LVACTIVE = (ushort) ( 1 ) ; 
                    }
                
                __context__.SourceCodeLine = 70;
                } 
            
            __context__.SourceCodeLine = 75;
            if ( Functions.TestForTrue  ( ( LVACTIVE)  ) ) 
                {
                __context__.SourceCodeLine = 76;
                return (ushort)( LVACTIVE) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 78;
                return (ushort)( 0) ; 
                }
            
            
            return 0; // default return value (none specified in module)
            }
            
        private void FORMATINPUTQUEUE (  SplusExecutionContext __context__, ushort LVINPUT , ushort LVACTIVE ) 
            { 
            ushort LVQUEUE = 0;
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 83;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( LVACTIVE ) && Functions.TestForTrue ( Functions.Not( CHECKQUEUEINPUTS( __context__ , (ushort)( LVINPUT ) ) ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 85;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 16 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)2; 
                int __FN_FORSTEP_VAL__1 = (int)Functions.ToLongInteger( -( 1 ) ); 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 87;
                    GVPOPUPQUEUE [ LVCOUNTER] = (ushort) ( GVPOPUPQUEUE[ (LVCOUNTER - 1) ] ) ; 
                    __context__.SourceCodeLine = 85;
                    } 
                
                __context__.SourceCodeLine = 89;
                GVPOPUPQUEUE [ 1] = (ushort) ( LVINPUT ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 91;
                if ( Functions.TestForTrue  ( ( Functions.Not( LVACTIVE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 93;
                    ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__2 = (ushort)16; 
                    int __FN_FORSTEP_VAL__2 = (int)1; 
                    for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                        { 
                        __context__.SourceCodeLine = 95;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVPOPUPQUEUE[ LVCOUNTER ] == LVINPUT))  ) ) 
                            {
                            __context__.SourceCodeLine = 96;
                            GVPOPUPQUEUE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                            }
                        
                        __context__.SourceCodeLine = 93;
                        } 
                    
                    __context__.SourceCodeLine = 98;
                    ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__3 = (ushort)(16 - 1); 
                    int __FN_FORSTEP_VAL__3 = (int)1; 
                    for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                        { 
                        __context__.SourceCodeLine = 100;
                        if ( Functions.TestForTrue  ( ( Functions.Not( GVPOPUPQUEUE[ LVCOUNTER ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 102;
                            GVPOPUPQUEUE [ LVCOUNTER] = (ushort) ( GVPOPUPQUEUE[ (LVCOUNTER + 1) ] ) ; 
                            __context__.SourceCodeLine = 103;
                            GVPOPUPQUEUE [ (LVCOUNTER + 1)] = (ushort) ( 0 ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 98;
                        } 
                    
                    } 
                
                }
            
            
            }
            
        object POPUPINPUT_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort LVINDEX = 0;
                ushort LVCOUNTER = 0;
                
                
                __context__.SourceCodeLine = 114;
                LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 115;
                FORMATINPUTQUEUE (  __context__ , (ushort)( LVINDEX ), (ushort)( POPUPINPUT[ LVINDEX ] .Value )) ; 
                __context__.SourceCodeLine = 116;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)16; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 118;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != GVPOPUPQUEUE[ 1 ]))  ) ) 
                        {
                        __context__.SourceCodeLine = 119;
                        POPUPOUTPUT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                        }
                    
                    __context__.SourceCodeLine = 116;
                    } 
                
                __context__.SourceCodeLine = 121;
                if ( Functions.TestForTrue  ( ( GVPOPUPQUEUE[ 1 ])  ) ) 
                    {
                    __context__.SourceCodeLine = 122;
                    POPUPOUTPUT [ GVPOPUPQUEUE[ 1 ]]  .Value = (ushort) ( 1 ) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    
    public override void LogosSplusInitialize()
    {
        _SplusNVRAM = new SplusNVRAM( this );
        GVPOPUPQUEUE  = new ushort[ 17 ];
        
        POPUPINPUT = new InOutArray<DigitalInput>( 16, this );
        for( uint i = 0; i < 16; i++ )
        {
            POPUPINPUT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( POPUPINPUT__DigitalInput__ + i, POPUPINPUT__DigitalInput__, this );
            m_DigitalInputList.Add( POPUPINPUT__DigitalInput__ + i, POPUPINPUT[i+1] );
        }
        
        POPUPOUTPUT = new InOutArray<DigitalOutput>( 16, this );
        for( uint i = 0; i < 16; i++ )
        {
            POPUPOUTPUT[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( POPUPOUTPUT__DigitalOutput__ + i, this );
            m_DigitalOutputList.Add( POPUPOUTPUT__DigitalOutput__ + i, POPUPOUTPUT[i+1] );
        }
        
        
        for( uint i = 0; i < 16; i++ )
            POPUPINPUT[i+1].OnDigitalChange.Add( new InputChangeHandlerWrapper( POPUPINPUT_OnChange_0, false ) );
            
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_IESS_POPUPMODULE_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint POPUPINPUT__DigitalInput__ = 0;
    const uint POPUPOUTPUT__DigitalOutput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
