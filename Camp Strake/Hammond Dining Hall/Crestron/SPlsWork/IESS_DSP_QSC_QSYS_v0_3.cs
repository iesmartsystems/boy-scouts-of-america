using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DSP_QSC_QSYS_V0_3
{
    public class UserModuleClass_IESS_DSP_QSC_QSYS_V0_3 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput LOGIN_NAME;
        Crestron.Logos.SplusObjects.StringInput LOGIN_PASSWORD;
        Crestron.Logos.SplusObjects.StringInput RX;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEUP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEDOWN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTETOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_0;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_1;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_2;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_3;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_4;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_5;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_6;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_7;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_8;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_9;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_STAR;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_POUND;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_BACKSPACE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWEROFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DIAL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_END;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_ACCEPT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DECLINE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_JOIN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_CONFERENCE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_REDIAL;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESET;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESTEP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> GROUP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOIP_CALLAPPEARANCE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> INSTANCETAGS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> VOIP_DIALENTRY;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOLUMEMUTE_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLSTATUS_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLINCOMING_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_AUTOANSWER_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_DND_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VOLUMELEVEL_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> GROUP_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_DIALSTRING;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNAME_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNUM;
        SplusTcpClient AUDIOCLIENT;
        SAUDIO AUDIODEVICE;
        ushort GVVOIPCOUNTER = 0;
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 120;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 122;
                LVSTATUS = (short) ( Functions.SocketDisconnectClient( AUDIOCLIENT ) ) ; 
                __context__.SourceCodeLine = 123;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 124;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 126;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 128;
                    LVSTATUS = (short) ( Functions.SocketConnectClient( AUDIOCLIENT , AUDIODEVICE.IPADDRESS , (ushort)( AUDIODEVICE.IPPORT ) , (ushort)( 1 ) ) ) ; 
                    } 
                
                }
            
            
            }
            
        private short VOLUMECONVERTER (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , ushort LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMEMULTIPLIER = 0;
            
            short LVVOLUMELEVEL = 0;
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 138;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 140;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 142;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 143;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 145;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 146;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 148;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 149;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 152;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 154;
            LVVOLUMEMULTIPLIER = (uint) ( ((LVVOLUMEINCOMING * 100) / LVBARGRAPHMAX) ) ; 
            __context__.SourceCodeLine = 155;
            LVVOLUMEMULTIPLIER = (uint) ( (LVVOLUMEMULTIPLIER * LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 156;
            LVVOLUMELEVEL = (short) ( (LVVOLUMEMULTIPLIER / 100) ) ; 
            __context__.SourceCodeLine = 158;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 159;
                LVVOLUMELEVEL = (short) ( (LVVOLUMELEVEL + LVMINIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 161;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < LVMINIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 162;
                LVVOLUMELEVEL = (short) ( LVMINIMUM ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 163;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVMAXIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 164;
                    LVVOLUMELEVEL = (short) ( LVMAXIMUM ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 166;
            return (short)( LVVOLUMELEVEL) ; 
            
            }
            
        private uint VOLUMECONVERTERREVERSE (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , short LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            ushort LVINC = 0;
            ushort LVMULT = 0;
            
            uint LVVOLUMELEVEL = 0;
            
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 174;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 176;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 178;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 179;
                LVINC = (ushort) ( (LVVOLUMEINCOMING + LVFMIN) ) ; 
                __context__.SourceCodeLine = 180;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 182;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 183;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 185;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 186;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 190;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                __context__.SourceCodeLine = 191;
                LVINC = (ushort) ( LVVOLUMEINCOMING ) ; 
                } 
            
            __context__.SourceCodeLine = 194;
            LVMULT = (ushort) ( (LVBARGRAPHMAX / LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 195;
            LVVOLUMELEVEL = (uint) ( (LVINC * LVMULT) ) ; 
            __context__.SourceCodeLine = 197;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 198;
                LVVOLUMELEVEL = (uint) ( 0 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 199;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVBARGRAPHMAX ))  ) ) 
                    {
                    __context__.SourceCodeLine = 200;
                    LVVOLUMELEVEL = (uint) ( LVBARGRAPHMAX ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 201;
            return (uint)( LVVOLUMELEVEL) ; 
            
            }
            
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 205;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 207;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 208;
                    Trace( "QSYS RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 210;
                    Trace( "QSYS TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            CrestronString LVTEMP;
            CrestronString LVTRASH;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            ushort LVINDEX = 0;
            ushort LVINDEX2ND = 0;
            
            
            __context__.SourceCodeLine = 217;
            LVTEMP  .UpdateValue ( LVSTRING + AUDIODEVICE . ETX  ) ; 
            __context__.SourceCodeLine = 218;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 1710))  ) ) 
                {
                __context__.SourceCodeLine = 219;
                Functions.SocketSend ( AUDIOCLIENT , LVTEMP ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 221;
                TX  .UpdateValue ( LVTEMP  ) ; 
                }
            
            __context__.SourceCodeLine = 222;
            SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
            
            }
            
        private void VOIP_NUM (  SplusExecutionContext __context__, ushort LVINDEX , CrestronString LVNUM ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 227;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOIPCALLSTATE[ LVINDEX ] ))  ) ) 
                {
                __context__.SourceCodeLine = 228;
                AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ]  .UpdateValue ( AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ] + LVNUM  ) ; 
                }
            
            else 
                { 
                __context__.SourceCodeLine = 231;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ LVINDEX ] ]  ) ; 
                __context__.SourceCodeLine = 232;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.pinpad." + LVNUM + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 233;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 234;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            
            }
            
        private void POLLINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVINDEXCOUNTER = 0;
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 241;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  ) ; 
            __context__.SourceCodeLine = 242;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
            __context__.SourceCodeLine = 243;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 244;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022 }, { \u0022Name\u0022: \u0022mute\u0022 }"  ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 245;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 247;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 249;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 251;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022 }, { \u0022Name\u0022: \u0022call.autoanswer\u0022 }, { \u0022Name\u0022: \u0022call.number\u0022 }, "  ) ; 
                        __context__.SourceCodeLine = 252;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.offhook\u0022 }, { \u0022Name\u0022: \u0022call.ringing\u0022 }, { \u0022Name\u0022: \u0022call.status\u0022 }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 254;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 257;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 259;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 260;
                                LVSTRING  .UpdateValue ( LVSTRING + ".gain\u0022 }"  ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 262;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 264;
                                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".gain\u0022 }, { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".mute\u0022 }"  ) ; 
                                    __context__.SourceCodeLine = 265;
                                    LVSTRING  .UpdateValue ( LVSTRING + ", { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".open\u0022 }, { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".config\u0022 }"  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 267;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 269;
                                        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                        ushort __FN_FOREND_VAL__1 = (ushort)AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ]; 
                                        int __FN_FORSTEP_VAL__1 = (int)1; 
                                        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                            { 
                                            __context__.SourceCodeLine = 271;
                                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u0022 }"  ) ; 
                                            __context__.SourceCodeLine = 272;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNTER < AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 273;
                                                LVSTRING  .UpdateValue ( LVSTRING + ", "  ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 269;
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 276;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            { 
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 279;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 280;
            SETQUEUE (  __context__ , LVSTRING) ; 
            
            }
            
        private void INITIALIZEINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVINDEXCOUNTER = 0;
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 286;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) > 2 ))  ) ) 
                { 
                __context__.SourceCodeLine = 288;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022ChangeGroup.AddComponentControl\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022, \u0022Component\u0022 : "  ) ; 
                __context__.SourceCodeLine = 289;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 290;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 292;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022 }, { \u0022Name\u0022: \u0022mute\u0022 }"  ) ; 
                    __context__.SourceCodeLine = 299;
                    AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 301;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 303;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }"  ) ; 
                        __context__.SourceCodeLine = 304;
                        AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 306;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 308;
                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 309;
                            LVSTRING  .UpdateValue ( LVSTRING + ".gain\u0022 }"  ) ; 
                            __context__.SourceCodeLine = 310;
                            AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 312;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 314;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".gain\u0022 }, { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".mute\u0022 }"  ) ; 
                                __context__.SourceCodeLine = 315;
                                LVSTRING  .UpdateValue ( LVSTRING + ", { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".open\u0022 }, { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".config\u0022 }"  ) ; 
                                __context__.SourceCodeLine = 323;
                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 325;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 328;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 330;
                                        GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                        __context__.SourceCodeLine = 331;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 332;
                                            AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 333;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022 }, { \u0022Name\u0022: \u0022call.autoanswer\u0022 }, { \u0022Name\u0022: \u0022call.number\u0022 }, "  ) ; 
                                        __context__.SourceCodeLine = 334;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.offhook\u0022 }, { \u0022Name\u0022: \u0022call.ringing\u0022 }, { \u0022Name\u0022: \u0022call.status\u0022 },"  ) ; 
                                        __context__.SourceCodeLine = 335;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022recent.calls\u0022 }"  ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 337;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "POTS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 339;
                                            GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                            __context__.SourceCodeLine = 340;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 341;
                                                AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                                }
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 343;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 345;
                                                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                                ushort __FN_FOREND_VAL__1 = (ushort)AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ]; 
                                                int __FN_FORSTEP_VAL__1 = (int)1; 
                                                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                                    { 
                                                    __context__.SourceCodeLine = 347;
                                                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u0022 }"  ) ; 
                                                    __context__.SourceCodeLine = 348;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNTER < AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 349;
                                                        LVSTRING  .UpdateValue ( LVSTRING + ", "  ) ; 
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 345;
                                                    } 
                                                
                                                __context__.SourceCodeLine = 351;
                                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 353;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } } }"  ) ; 
                __context__.SourceCodeLine = 354;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 355;
                POLLINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                } 
            
            
            }
            
        private void DEVICEONLINE (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 361;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)64; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 363;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVCOUNTER )) ; 
                __context__.SourceCodeLine = 361;
                } 
            
            __context__.SourceCodeLine = 365;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_7__" , 200 , __SPLS_TMPVAR__WAITLABEL_7___Callback ) ;
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_7___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            {
            __context__.SourceCodeLine = 366;
            SETQUEUE (  __context__ , AUDIODEVICE.COMMANDAUTOPOLL) ; 
            }
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void CHECKINSTANCETAGS (  SplusExecutionContext __context__, CrestronString LVINSTANCETAG , CrestronString LVPARSEDSTRING ) 
        { 
        ushort LVCOUNTER = 0;
        ushort LVVOIPNUM = 0;
        ushort LVVOIPCOUNTER = 0;
        
        short LVVOL = 0;
        
        CrestronString LVVOLUME;
        CrestronString LVSTRING;
        CrestronString LVTRASH;
        CrestronString LVINSTANCEINDEX;
        CrestronString LVCOMPARESTRING;
        LVVOLUME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        LVINSTANCEINDEX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        LVCOMPARESTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 373;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 375;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINSTANCETAG == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                { 
                __context__.SourceCodeLine = 377;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 379;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022gain\u0022" , LVPARSEDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 381;
                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                        __context__.SourceCodeLine = 382;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022gain\u0022" , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 383;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 385;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 386;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 388;
                                LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 389;
                                LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                __context__.SourceCodeLine = 390;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 391;
                                    LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                    }
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 393;
                                    LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                    }
                                
                                __context__.SourceCodeLine = 394;
                                AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                __context__.SourceCodeLine = 395;
                                VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                __context__.SourceCodeLine = 396;
                                if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 397;
                                    AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                    }
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 401;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022mute\u0022" , LVPARSEDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 403;
                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                        __context__.SourceCodeLine = 404;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022gain\u0022" , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 405;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 407;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 408;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 410;
                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 411;
                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 413;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 415;
                                    AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 416;
                                    VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                    } 
                                
                                }
                            
                            } 
                        
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 421;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 423;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022output." , LVPARSEDSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 425;
                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                            __context__.SourceCodeLine = 426;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022output." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 427;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 428;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                            __context__.SourceCodeLine = 429;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 431;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "gain\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 433;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 434;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "gain\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 435;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 437;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 438;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 440;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 441;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 442;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 443;
                                                LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 445;
                                                LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 446;
                                            AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                            __context__.SourceCodeLine = 447;
                                            VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                            __context__.SourceCodeLine = 448;
                                            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 449;
                                                AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 453;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "mute\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 455;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 456;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "mute\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 457;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 459;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 460;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 461;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 463;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 464;
                                            VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 466;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 468;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 469;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 475;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "wall." , LVPARSEDSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 477;
                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                            __context__.SourceCodeLine = 478;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022wall." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 479;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 480;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                            __context__.SourceCodeLine = 481;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 483;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "open\u0022,\u0022String\u0022:\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 485;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 486;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "open\u0022,\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 487;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 489;
                                        AUDIODEVICE . STATUSWALL [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 491;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 493;
                                            AUDIODEVICE . STATUSWALL [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 494;
                                            AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 495;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 498;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "config\u0022,\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 500;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 501;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "config\u0022,\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 502;
                                    LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 503;
                                    if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSWALL[ LVCOUNTER ])  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 505;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "G" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 507;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "G" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 508;
                                            AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                            __context__.SourceCodeLine = 509;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 516;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 518;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022select." , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 520;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 521;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022select." , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 522;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 523;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                __context__.SourceCodeLine = 524;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 526;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 528;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 529;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 531;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 532;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 1), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 533;
                                            AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( Functions.Atoi( LVVOLUME ) ) ; 
                                            __context__.SourceCodeLine = 534;
                                            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOLUMEMUTE[ LVCOUNTER ] ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 535;
                                                GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            __context__.SourceCodeLine = 540;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022mute." , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 542;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 543;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022mute." , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 544;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 545;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                __context__.SourceCodeLine = 546;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 548;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 550;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 551;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 553;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 554;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 556;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 558;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 559;
                                                GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 565;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 567;
                                LVCOMPARESTRING  .UpdateValue ( "\u0022Name\u0022:\u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVCOUNTER ] ) ) + ".gain\u0022"  ) ; 
                                __context__.SourceCodeLine = 568;
                                if ( Functions.TestForTrue  ( ( Functions.Find( LVCOMPARESTRING , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 570;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 571;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( LVCOMPARESTRING , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 572;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 574;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 575;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 577;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 578;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 579;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 580;
                                                LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 582;
                                                LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 583;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVVOL == Functions.ToSignedLongInteger( -( 100 ) )))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 585;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 586;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 590;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 591;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 597;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 599;
                                    ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                                    ushort __FN_FOREND_VAL__2 = (ushort)2; 
                                    int __FN_FORSTEP_VAL__2 = (int)1; 
                                    for ( LVVOIPCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVVOIPCOUNTER  >= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVVOIPCOUNTER  <= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVVOIPCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                                        { 
                                        __context__.SourceCodeLine = 601;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER == AUDIODEVICE.VOIPINSTANCEINDEX[ LVVOIPCOUNTER ]))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 603;
                                            LVVOIPNUM = (ushort) ( LVVOIPCOUNTER ) ; 
                                            __context__.SourceCodeLine = 604;
                                            break ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 599;
                                        } 
                                    
                                    __context__.SourceCodeLine = 607;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.dnd\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 609;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 610;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.dnd\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 611;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 613;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 614;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 615;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 616;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "on" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 618;
                                                AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 619;
                                                VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 621;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "off" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 623;
                                                    AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 624;
                                                    VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 628;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.autoanswer\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 630;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 631;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.autoanswer\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 632;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 634;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 635;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 636;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 637;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "on" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 639;
                                                AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 640;
                                                VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 642;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "off" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 644;
                                                    AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 645;
                                                    VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 649;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.offhook\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 651;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 652;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.offhook\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 653;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 655;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 656;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 657;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 658;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 660;
                                                AUDIODEVICE . STATUSVOIPCALLSTATE [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 661;
                                                VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 663;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 665;
                                                    AUDIODEVICE . STATUSVOIPCALLSTATE [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 666;
                                                    VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 684;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.status\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 686;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 687;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.status\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 688;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 690;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 691;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 692;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Dialing - " , LVSTRING ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 693;
                                                VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 694;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Incoming Call" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 696;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "Incoming Call" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 697;
                                                VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 698;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( " - " , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 700;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( " - " , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 701;
                                                    VOIP_CALLINCOMINGNAME_FB [ LVVOIPNUM]  .UpdateValue ( LVSTRING  ) ; 
                                                    } 
                                                
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 705;
                                                VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 708;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022recent.calls\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 710;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 711;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022recent.calls\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 712;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Choices\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 714;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Text\u005C\u0022:\u005C\u0022" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 716;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "Text\u005C\u0022:\u005C\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 717;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( "\u005C\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 718;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 2), LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 719;
                                                AUDIODEVICE . STATUSVOIPRECENTCALL [ LVVOIPNUM ]  .UpdateValue ( LVSTRING  ) ; 
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 724;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 726;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 727;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022load." , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 729;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022load." , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 730;
                                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 731;
                                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                            __context__.SourceCodeLine = 732;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Color\u0022:\u0022@" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 734;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Color\u0022:\u0022@" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 735;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 736;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 737;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "7F7F"))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 738;
                                                    GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( Functions.Atoi( LVINSTANCEINDEX ) ) ; 
                                                    }
                                                
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            __context__.SourceCodeLine = 373;
            } 
        
        
        }
        
    private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
        { 
        ushort LVINDEX = 0;
        ushort LVCOUNTER = 0;
        ushort LVVOIPNUM = 0;
        ushort LVVOIPCOUNTER = 0;
        
        short LVVOL = 0;
        
        CrestronString LVRX;
        CrestronString LVTRASH;
        CrestronString LVSTRING;
        CrestronString LVTAG;
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
        LVTAG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        
        
        __context__.SourceCodeLine = 750;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u0000" , AUDIODEVICE.RXQUEUE ))  ) ) 
            { 
            __context__.SourceCodeLine = 752;
            LVRX  .UpdateValue ( Functions.Remove ( "\u0000" , AUDIODEVICE . RXQUEUE )  ) ; 
            __context__.SourceCodeLine = 753;
            LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
            __context__.SourceCodeLine = 754;
            SETDEBUG (  __context__ , LVRX, (ushort)( 1 )) ; 
            __context__.SourceCodeLine = 755;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSCOMMUNICATING ))  ) ) 
                {
                __context__.SourceCodeLine = 756;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 1 ) ; 
                }
            
            __context__.SourceCodeLine = 757;
            if ( Functions.TestForTrue  ( ( Functions.Find( "Changes\u0022:[{" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 759;
                LVTRASH  .UpdateValue ( Functions.Remove ( "Changes\u0022:[{" , LVRX )  ) ; 
                __context__.SourceCodeLine = 760;
                while ( Functions.TestForTrue  ( ( Functions.Find( "Component\u0022:\u0022" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 762;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "Component\u0022:\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 763;
                    LVTAG  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 764;
                    LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                    __context__.SourceCodeLine = 765;
                    if ( Functions.TestForTrue  ( ( Functions.Find( ",{\u0022Component\u0022:\u0022" , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 767;
                        LVSTRING  .UpdateValue ( Functions.Remove ( ",{\u0022Component\u0022:\u0022" , LVRX )  ) ; 
                        __context__.SourceCodeLine = 768;
                        LVRX  .UpdateValue ( ",{\u0022Component\u0022:\u0022" + LVRX  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 771;
                        LVSTRING  .UpdateValue ( LVRX  ) ; 
                        }
                    
                    __context__.SourceCodeLine = 772;
                    CHECKINSTANCETAGS (  __context__ , LVTAG, LVSTRING) ; 
                    __context__.SourceCodeLine = 760;
                    } 
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 775;
                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022result\u0022:{\u0022Name\u0022:\u0022" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 777;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022result\u0022:{\u0022Name\u0022:\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 778;
                    LVTAG  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 779;
                    LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                    __context__.SourceCodeLine = 780;
                    CHECKINSTANCETAGS (  __context__ , LVTAG, LVRX) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 750;
            } 
        
        
        }
        
    object AUDIOCLIENT_OnSocketConnect_0 ( Object __Info__ )
    
        { 
        SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 791;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 792;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 793;
            CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
            __context__.SourceCodeLine = 794;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.LOGINPASSWORD ) > 3 ))  ) ) 
                { 
                __context__.SourceCodeLine = 796;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022:\u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022:\u0022Logon\u0022, \u0022params\u0022:{ \u0022User\u0022:\u0022" + AUDIODEVICE . LOGINNAME + "\u0022, "  ) ; 
                __context__.SourceCodeLine = 797;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022Password\u0022:\u0022" + AUDIODEVICE . LOGINPASSWORD + "\u0022 } }"  ) ; 
                __context__.SourceCodeLine = 798;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            __context__.SourceCodeLine = 800;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_8__" , 200 , __SPLS_TMPVAR__WAITLABEL_8___Callback ) ;
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SocketInfo__ ); }
        return this;
        
    }
    
public void __SPLS_TMPVAR__WAITLABEL_8___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 801;
            DEVICEONLINE (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object AUDIOCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 806;
        AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 807;
        AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 808;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 809;
        CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 813;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 814;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 816;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 817;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 821;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 822;
            AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 823;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 829;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + AUDIOCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 830;
        Functions.ClearBuffer ( AUDIOCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 831;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.RXQUEUE ) > 2 ))  ) ) 
            {
            __context__.SourceCodeLine = 832;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 836;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 838;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 839;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 843;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 844;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 849;
        SETQUEUE (  __context__ , AUDIODEVICE.COMMANDPOLL) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 853;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + RX  ) ; 
        __context__.SourceCodeLine = 854;
        PARSEFEEDBACK (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 858;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 862;
        AUDIODEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 866;
        AUDIODEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_NAME_OnChange_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 870;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( LOGIN_NAME  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_PASSWORD_OnChange_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 874;
        AUDIODEVICE . LOGINPASSWORD  .UpdateValue ( LOGIN_PASSWORD  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object INSTANCETAGS_OnChange_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 880;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 881;
        LVSTRING  .UpdateValue ( INSTANCETAGS [ LVINDEX ]  ) ; 
        __context__.SourceCodeLine = 882;
        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
            { 
            __context__.SourceCodeLine = 884;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( "-" , LVSTRING )  ) ; 
            __context__.SourceCodeLine = 885;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( (Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) - 1), AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] )  ) ; 
            __context__.SourceCodeLine = 886;
            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                { 
                __context__.SourceCodeLine = 888;
                AUDIODEVICE . INSTANCETAGSINDEX [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                __context__.SourceCodeLine = 889;
                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 891;
                    AUDIODEVICE . INSTANCETAGSINDEXSECOND [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                    __context__.SourceCodeLine = 892;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 895;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    }
                
                } 
            
            __context__.SourceCodeLine = 897;
            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCOMMUNICATING)  ) ) 
                { 
                __context__.SourceCodeLine = 899;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEUP_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 906;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 907;
        while ( Functions.TestForTrue  ( ( VOLUMEUP[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 909;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 910;
            CreateWait ( "VOLUP" , 10 , VOLUP_Callback ) ;
            __context__.SourceCodeLine = 907;
            } 
        
        __context__.SourceCodeLine = 930;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLUP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            CrestronString LVSTRINGVOL;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            __context__.SourceCodeLine = 913;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) > AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 915;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 916;
                MakeString ( LVSTRINGVOL , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 917;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 918;
                LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 919;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 920;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 921;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 922;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 923;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 924;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 925;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                __context__.SourceCodeLine = 926;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEDOWN_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 934;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 935;
        while ( Functions.TestForTrue  ( ( VOLUMEDOWN[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 937;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 938;
            CreateWait ( "VOLDN" , 10 , VOLDN_Callback ) ;
            __context__.SourceCodeLine = 935;
            } 
        
        __context__.SourceCodeLine = 958;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLDN_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            CrestronString LVSTRINGVOL;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            __context__.SourceCodeLine = 941;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) < AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 943;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 944;
                MakeString ( LVSTRINGVOL , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 945;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 946;
                LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 947;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 948;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 949;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 950;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 951;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 952;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 953;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                __context__.SourceCodeLine = 954;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEMUTEON_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 963;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 964;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 966;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 967;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 968;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 970;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 972;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 973;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 974;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 976;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 980;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 982;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                        __context__.SourceCodeLine = 983;
                        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                        __context__.SourceCodeLine = 984;
                        LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: -100 }"  ) ; 
                        __context__.SourceCodeLine = 985;
                        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 987;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 991;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 992;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTEOFF_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 997;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 998;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1000;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1001;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
            __context__.SourceCodeLine = 1002;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1004;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1006;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1007;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1008;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1010;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1013;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1015;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                        __context__.SourceCodeLine = 1016;
                        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                        __context__.SourceCodeLine = 1017;
                        LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: 0 }"  ) ; 
                        __context__.SourceCodeLine = 1018;
                        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1020;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 1023;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1024;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTETOGGLE_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1029;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1030;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOLUMEMUTE[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1032;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1034;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1035;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1036;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1038;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1040;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1041;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                    __context__.SourceCodeLine = 1042;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1044;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1047;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1049;
                            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                            __context__.SourceCodeLine = 1050;
                            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1051;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: 0 }"  ) ; 
                            __context__.SourceCodeLine = 1052;
                            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1054;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1057;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1058;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1062;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1064;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1065;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 1066;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1068;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1070;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1071;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    __context__.SourceCodeLine = 1072;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1074;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1077;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1079;
                            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                            __context__.SourceCodeLine = 1080;
                            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1081;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: -100 }"  ) ; 
                            __context__.SourceCodeLine = 1082;
                            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1084;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1087;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1088;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESET_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        CrestronString LVSTRINGVOL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        
        short LVVOL = 0;
        
        
        __context__.SourceCodeLine = 1095;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1096;
        LVVOL = (short) ( VOLUMECONVERTER( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) , (ushort)( VOLUMESET[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) ) ; 
        __context__.SourceCodeLine = 1097;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOL >= AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOL <= AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1099;
            MakeString ( LVSTRINGVOL , "{0:d}", (short)LVVOL) ; 
            __context__.SourceCodeLine = 1100;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1102;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1103;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1104;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1106;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1108;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1109;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    __context__.SourceCodeLine = 1110;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 1112;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1113;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object GROUP_OnChange_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        CrestronString LVWALLCONFIG;
        CrestronString LVNAME;
        CrestronString LVSTRINGWALL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVWALLCONFIG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVSTRINGWALL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1120;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1121;
        LVNAME  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
        __context__.SourceCodeLine = 1122;
        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
            { 
            __context__.SourceCodeLine = 1124;
            if ( Functions.TestForTrue  ( ( Functions.Not( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ))  ) ) 
                { 
                __context__.SourceCodeLine = 1126;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1127;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".open\u0022, \u0022Value\u0022: 0 },"  ) ; 
                __context__.SourceCodeLine = 1128;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".config\u0022, \u0022String\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1129;
                LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022 } ] } }"  ) ; 
                __context__.SourceCodeLine = 1130;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1131;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 1136;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)64; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 1138;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1140;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]) ) && Functions.TestForTrue ( Functions.BoolToInt (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue != AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1142;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != AUDIODEVICE.LASTINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1144;
                                LVWALLCONFIG  .UpdateValue ( LVWALLCONFIG + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " "  ) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1136;
                    } 
                
                __context__.SourceCodeLine = 1149;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)64; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 1151;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1153;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]) ) && Functions.TestForTrue ( Functions.BoolToInt (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue != AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1155;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != AUDIODEVICE.LASTINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1157;
                                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ]  ) ; 
                                __context__.SourceCodeLine = 1158;
                                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".open\u0022, \u0022Value\u0022: 1 },"  ) ; 
                                __context__.SourceCodeLine = 1159;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".config\u0022, \u0022Value\u0022: \u0022"  ) ; 
                                __context__.SourceCodeLine = 1160;
                                LVSTRING  .UpdateValue ( LVSTRING + LVWALLCONFIG + "G" + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ) + "\u0022 } ] } }"  ) ; 
                                __context__.SourceCodeLine = 1161;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 1162;
                                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1149;
                    } 
                
                __context__.SourceCodeLine = 1167;
                LVWALLCONFIG  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 1168;
                AUDIODEVICE . STATUSGROUP [ AUDIODEVICE.LASTINDEX] = (ushort) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
                __context__.SourceCodeLine = 1169;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)64; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 1171;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1173;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1175;
                            LVWALLCONFIG  .UpdateValue ( LVWALLCONFIG + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " "  ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1169;
                    } 
                
                __context__.SourceCodeLine = 1179;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1180;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".open\u0022, \u0022Value\u0022: 1 },"  ) ; 
                __context__.SourceCodeLine = 1181;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".config\u0022, \u0022Value\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1182;
                LVSTRING  .UpdateValue ( LVSTRING + LVWALLCONFIG + "G" + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + "\u0022 } ] } }"  ) ; 
                __context__.SourceCodeLine = 1183;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1184;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1188;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1190;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1191;
                if ( Functions.TestForTrue  ( ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue)  ) ) 
                    { 
                    __context__.SourceCodeLine = 1193;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: "  ) ; 
                    __context__.SourceCodeLine = 1194;
                    LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + " }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: 0 }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1197;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    }
                
                __context__.SourceCodeLine = 1198;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 1199;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1200;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1202;
                if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1204;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1205;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    __context__.SourceCodeLine = 1206;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    __context__.SourceCodeLine = 1207;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 1208;
                    POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1210;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1218;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESTEP_OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1229;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1230;
        AUDIODEVICE . VOLUMESTEP [ AUDIODEVICE.LASTINDEX] = (ushort) ( VOLUMESTEP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERTOGGLE_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1235;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1236;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1237;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 1238;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPAUTOANSWER[ AUDIODEVICE.LASTINDEX ])  ) ) 
            {
            __context__.SourceCodeLine = 1239;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 0 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1241;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 1 }"  ) ; 
            }
        
        __context__.SourceCodeLine = 1242;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1243;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1244;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERON_OnPush_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1249;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1250;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1251;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1252;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1253;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1254;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWEROFF_OnPush_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1259;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1260;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1261;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1262;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1263;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1264;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDTOGGLE_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1269;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1270;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1271;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 1272;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPDND[ AUDIODEVICE.LASTINDEX ])  ) ) 
            {
            __context__.SourceCodeLine = 1273;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 0 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1275;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 1 }"  ) ; 
            }
        
        __context__.SourceCodeLine = 1276;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1277;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1278;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDON_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1283;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1284;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1285;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1286;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1287;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1288;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDOFF_OnPush_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1293;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1294;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1295;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1296;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1297;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1298;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_ACCEPT_OnPush_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1303;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1304;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1305;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1306;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1307;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1308;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DECLINE_OnPush_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1313;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1314;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1315;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.disconnect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1316;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1317;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1318;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_JOIN_OnPush_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1323;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1324;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1325;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022hook.flash\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1326;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1327;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1328;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CONFERENCE_OnPush_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_REDIAL_OnPush_31 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1336;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1337;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.STATUSVOIPRECENTCALL[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1339;
            VOIP_DIALSTRING [ AUDIODEVICE.LASTINDEX]  .UpdateValue ( AUDIODEVICE . STATUSVOIPRECENTCALL [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1340;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1341;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.number\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . STATUSVOIPRECENTCALL [ AUDIODEVICE.LASTINDEX ] + "\u0022 }"  ) ; 
            __context__.SourceCodeLine = 1342;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1343;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1344;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1345;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1346;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1347;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1348;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_0_OnPush_32 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1354;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "0") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_1_OnPush_33 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1358;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_2_OnPush_34 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1362;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "2") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_3_OnPush_35 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1366;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "3") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_4_OnPush_36 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1370;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "4") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_5_OnPush_37 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1374;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "5") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_6_OnPush_38 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1378;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "6") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_7_OnPush_39 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1382;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "7") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_8_OnPush_40 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1386;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "8") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_9_OnPush_41 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1390;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "9") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_STAR_OnPush_42 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1394;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "*") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_POUND_OnPush_43 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1398;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "#") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_BACKSPACE_OnPush_44 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIAL_OnPush_45 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1406;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1407;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.VOIPDIALSTRING[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1409;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1410;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.number\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ] + "\u0022 }"  ) ; 
            __context__.SourceCodeLine = 1411;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1412;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1413;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1414;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1415;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1416;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1417;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_END_OnPush_46 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1423;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1424;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1425;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.disconnect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1426;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1427;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1428;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIALENTRY_OnChange_47 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1432;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1433;
        AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ]  .UpdateValue ( VOIP_DIALENTRY [ AUDIODEVICE.LASTINDEX ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CALLAPPEARANCE_OnChange_48 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1444;
        GVVOIPCOUNTER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1445;
        AUDIODEVICE . IPPORT = (ushort) ( 1710 ) ; 
        __context__.SourceCodeLine = 1446;
        AUDIODEVICE . BAUD = (uint) ( 9600 ) ; 
        __context__.SourceCodeLine = 1447;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1448;
        AUDIODEVICE . ETX  .UpdateValue ( "\u0000"  ) ; 
        __context__.SourceCodeLine = 1449;
        AUDIODEVICE . COMMANDPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022StatusGet\u0022, \u0022params\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1450;
        AUDIODEVICE . COMMANDAUTOPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022ChangeGroup.AutoPoll\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022,\u0022Rate\u0022: 5 } }"  ) ; 
        __context__.SourceCodeLine = 1451;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 1453;
            AUDIODEVICE . VOLUMEMIN [ LVCOUNTER] = (short) ( Functions.ToInteger( -( 100 ) ) ) ; 
            __context__.SourceCodeLine = 1454;
            AUDIODEVICE . VOLUMEMAX [ LVCOUNTER] = (short) ( 20 ) ; 
            __context__.SourceCodeLine = 1455;
            AUDIODEVICE . VOLUMESTEP [ LVCOUNTER] = (ushort) ( 3 ) ; 
            __context__.SourceCodeLine = 1451;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    AUDIOCLIENT  = new SplusTcpClient ( 65534, this );
    AUDIODEVICE  = new SAUDIO( this, true );
    AUDIODEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    VOLUMEUP = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEUP[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP__DigitalInput__ + i, VOLUMEUP__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEUP__DigitalInput__ + i, VOLUMEUP[i+1] );
    }
    
    VOLUMEDOWN = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEDOWN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN[i+1] );
    }
    
    VOLUMEMUTETOGGLE = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTETOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE[i+1] );
    }
    
    VOLUMEMUTEON = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON[i+1] );
    }
    
    VOLUMEMUTEOFF = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF[i+1] );
    }
    
    VOIP_NUM_0 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_0[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0[i+1] );
    }
    
    VOIP_NUM_1 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_1[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1[i+1] );
    }
    
    VOIP_NUM_2 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_2[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2[i+1] );
    }
    
    VOIP_NUM_3 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_3[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3[i+1] );
    }
    
    VOIP_NUM_4 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_4[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4[i+1] );
    }
    
    VOIP_NUM_5 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_5[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5[i+1] );
    }
    
    VOIP_NUM_6 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_6[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6[i+1] );
    }
    
    VOIP_NUM_7 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_7[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7[i+1] );
    }
    
    VOIP_NUM_8 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_8[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8[i+1] );
    }
    
    VOIP_NUM_9 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_9[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9[i+1] );
    }
    
    VOIP_NUM_STAR = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_STAR[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR[i+1] );
    }
    
    VOIP_NUM_POUND = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_POUND[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND[i+1] );
    }
    
    VOIP_BACKSPACE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_BACKSPACE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE[i+1] );
    }
    
    VOIP_AUTOANSWERTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE[i+1] );
    }
    
    VOIP_AUTOANSWERON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON[i+1] );
    }
    
    VOIP_AUTOANSWEROFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWEROFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF[i+1] );
    }
    
    VOIP_DNDTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE[i+1] );
    }
    
    VOIP_DNDON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON[i+1] );
    }
    
    VOIP_DNDOFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF[i+1] );
    }
    
    VOIP_DIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL[i+1] );
    }
    
    VOIP_END = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_END[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_END__DigitalInput__ + i, VOIP_END__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_END__DigitalInput__ + i, VOIP_END[i+1] );
    }
    
    VOIP_ACCEPT = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_ACCEPT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT[i+1] );
    }
    
    VOIP_DECLINE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DECLINE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE[i+1] );
    }
    
    VOIP_JOIN = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_JOIN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN[i+1] );
    }
    
    VOIP_CONFERENCE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CONFERENCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE[i+1] );
    }
    
    VOIP_REDIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_REDIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL[i+1] );
    }
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    VOLUMEMUTE_FB = new InOutArray<DigitalOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTE_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOLUMEMUTE_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOLUMEMUTE_FB__DigitalOutput__ + i, VOLUMEMUTE_FB[i+1] );
    }
    
    VOIP_CALLSTATUS_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLSTATUS_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, VOIP_CALLSTATUS_FB[i+1] );
    }
    
    VOIP_CALLINCOMING_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMING_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, VOIP_CALLINCOMING_FB[i+1] );
    }
    
    VOIP_AUTOANSWER_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWER_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, VOIP_AUTOANSWER_FB[i+1] );
    }
    
    VOIP_DND_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DND_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_DND_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_DND_FB__DigitalOutput__ + i, VOIP_DND_FB[i+1] );
    }
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    VOLUMESET = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESET[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET[i+1] );
    }
    
    VOLUMESTEP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESTEP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP[i+1] );
    }
    
    GROUP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( GROUP__AnalogSerialInput__ + i, GROUP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( GROUP__AnalogSerialInput__ + i, GROUP[i+1] );
    }
    
    VOIP_CALLAPPEARANCE = new InOutArray<AnalogInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLAPPEARANCE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE[i+1] );
    }
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    VOLUMELEVEL_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMELEVEL_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, VOLUMELEVEL_FB[i+1] );
    }
    
    GROUP_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( GROUP_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( GROUP_FB__AnalogSerialOutput__ + i, GROUP_FB[i+1] );
    }
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 16, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    LOGIN_NAME = new Crestron.Logos.SplusObjects.StringInput( LOGIN_NAME__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_NAME__AnalogSerialInput__, LOGIN_NAME );
    
    LOGIN_PASSWORD = new Crestron.Logos.SplusObjects.StringInput( LOGIN_PASSWORD__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_PASSWORD__AnalogSerialInput__, LOGIN_PASSWORD );
    
    RX = new Crestron.Logos.SplusObjects.StringInput( RX__AnalogSerialInput__, 1023, this );
    m_StringInputList.Add( RX__AnalogSerialInput__, RX );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    INSTANCETAGS = new InOutArray<StringInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        INSTANCETAGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS[i+1] );
    }
    
    VOIP_DIALENTRY = new InOutArray<StringInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALENTRY[i+1] = new Crestron.Logos.SplusObjects.StringInput( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY[i+1] );
    }
    
    TX = new Crestron.Logos.SplusObjects.StringOutput( TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__AnalogSerialOutput__, TX );
    
    VOIP_DIALSTRING = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALSTRING[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_DIALSTRING__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_DIALSTRING__AnalogSerialOutput__ + i, VOIP_DIALSTRING[i+1] );
    }
    
    VOIP_CALLINCOMINGNAME_FB = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNAME_FB[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNAME_FB[i+1] );
    }
    
    VOIP_CALLINCOMINGNUM = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNUM[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNUM[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_7___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_7___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_8___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_8___CallbackFn );
    VOLUP_Callback = new WaitFunction( VOLUP_CallbackFn );
    VOLDN_Callback = new WaitFunction( VOLDN_CallbackFn );
    
    AUDIOCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketConnect_0, false ) );
    AUDIOCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketDisconnect_1, false ) );
    AUDIOCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketStatus_2, false ) );
    AUDIOCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_5, false ) );
    RX.OnSerialChange.Add( new InputChangeHandlerWrapper( RX_OnChange_6, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_7, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_8, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_9, false ) );
    LOGIN_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_NAME_OnChange_10, false ) );
    LOGIN_PASSWORD.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_PASSWORD_OnChange_11, false ) );
    for( uint i = 0; i < 64; i++ )
        INSTANCETAGS[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( INSTANCETAGS_OnChange_12, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEUP[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_OnPush_13, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEDOWN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_OnPush_14, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEON_OnPush_15, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEOFF_OnPush_16, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTETOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTETOGGLE_OnPush_17, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESET[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESET_OnChange_18, false ) );
        
    for( uint i = 0; i < 64; i++ )
        GROUP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( GROUP_OnChange_19, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESTEP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESTEP_OnChange_20, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERTOGGLE_OnPush_21, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERON_OnPush_22, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWEROFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWEROFF_OnPush_23, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDTOGGLE_OnPush_24, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDON_OnPush_25, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDOFF_OnPush_26, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_ACCEPT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_ACCEPT_OnPush_27, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DECLINE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DECLINE_OnPush_28, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_JOIN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_JOIN_OnPush_29, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CONFERENCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_CONFERENCE_OnPush_30, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_REDIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_REDIAL_OnPush_31, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_0[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_0_OnPush_32, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_1[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_1_OnPush_33, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_2[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_2_OnPush_34, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_3[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_3_OnPush_35, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_4[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_4_OnPush_36, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_5[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_5_OnPush_37, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_6[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_6_OnPush_38, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_7[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_7_OnPush_39, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_8[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_8_OnPush_40, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_9[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_9_OnPush_41, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_STAR[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_STAR_OnPush_42, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_POUND[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_POUND_OnPush_43, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_BACKSPACE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_BACKSPACE_OnPush_44, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DIAL_OnPush_45, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_END[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_END_OnPush_46, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIALENTRY[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( VOIP_DIALENTRY_OnChange_47, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CALLAPPEARANCE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOIP_CALLAPPEARANCE_OnChange_48, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DSP_QSC_QSYS_V0_3 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_7___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_8___Callback;
private WaitFunction VOLUP_Callback;
private WaitFunction VOLDN_Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint LOGIN_NAME__AnalogSerialInput__ = 2;
const uint LOGIN_PASSWORD__AnalogSerialInput__ = 3;
const uint RX__AnalogSerialInput__ = 4;
const uint MANUALCMD__AnalogSerialInput__ = 5;
const uint VOLUMEUP__DigitalInput__ = 3;
const uint VOLUMEDOWN__DigitalInput__ = 67;
const uint VOLUMEMUTETOGGLE__DigitalInput__ = 131;
const uint VOLUMEMUTEON__DigitalInput__ = 195;
const uint VOLUMEMUTEOFF__DigitalInput__ = 259;
const uint VOIP_NUM_0__DigitalInput__ = 323;
const uint VOIP_NUM_1__DigitalInput__ = 325;
const uint VOIP_NUM_2__DigitalInput__ = 327;
const uint VOIP_NUM_3__DigitalInput__ = 329;
const uint VOIP_NUM_4__DigitalInput__ = 331;
const uint VOIP_NUM_5__DigitalInput__ = 333;
const uint VOIP_NUM_6__DigitalInput__ = 335;
const uint VOIP_NUM_7__DigitalInput__ = 337;
const uint VOIP_NUM_8__DigitalInput__ = 339;
const uint VOIP_NUM_9__DigitalInput__ = 341;
const uint VOIP_NUM_STAR__DigitalInput__ = 343;
const uint VOIP_NUM_POUND__DigitalInput__ = 345;
const uint VOIP_BACKSPACE__DigitalInput__ = 347;
const uint VOIP_AUTOANSWERTOGGLE__DigitalInput__ = 349;
const uint VOIP_AUTOANSWERON__DigitalInput__ = 351;
const uint VOIP_AUTOANSWEROFF__DigitalInput__ = 353;
const uint VOIP_DNDTOGGLE__DigitalInput__ = 355;
const uint VOIP_DNDON__DigitalInput__ = 357;
const uint VOIP_DNDOFF__DigitalInput__ = 359;
const uint VOIP_DIAL__DigitalInput__ = 361;
const uint VOIP_END__DigitalInput__ = 363;
const uint VOIP_ACCEPT__DigitalInput__ = 365;
const uint VOIP_DECLINE__DigitalInput__ = 367;
const uint VOIP_JOIN__DigitalInput__ = 369;
const uint VOIP_CONFERENCE__DigitalInput__ = 371;
const uint VOIP_REDIAL__DigitalInput__ = 373;
const uint VOLUMESET__AnalogSerialInput__ = 6;
const uint VOLUMESTEP__AnalogSerialInput__ = 70;
const uint GROUP__AnalogSerialInput__ = 134;
const uint VOIP_CALLAPPEARANCE__AnalogSerialInput__ = 198;
const uint INSTANCETAGS__AnalogSerialInput__ = 200;
const uint VOIP_DIALENTRY__AnalogSerialInput__ = 264;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint TX__AnalogSerialOutput__ = 1;
const uint VOLUMEMUTE_FB__DigitalOutput__ = 1;
const uint VOIP_CALLSTATUS_FB__DigitalOutput__ = 65;
const uint VOIP_CALLINCOMING_FB__DigitalOutput__ = 67;
const uint VOIP_AUTOANSWER_FB__DigitalOutput__ = 69;
const uint VOIP_DND_FB__DigitalOutput__ = 71;
const uint VOLUMELEVEL_FB__AnalogSerialOutput__ = 2;
const uint GROUP_FB__AnalogSerialOutput__ = 66;
const uint VOIP_DIALSTRING__AnalogSerialOutput__ = 130;
const uint VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ = 132;
const uint VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ = 134;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SAUDIO : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSCOMMUNICATING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  [] STATUSVOLUMEMUTE;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  [] STATUSWALL;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  [] STATUSGROUP;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  [] STATUSVOIPAUTOANSWER;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  [] STATUSVOIPDND;
    
    [SplusStructAttribute(8, false, false)]
    public ushort  [] STATUSVOIPCALLSTATE;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  [] STATUSVOIPRECENTCALL;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  [] VOIPDIALSTRING;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  COMMANDPOLL;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  COMMANDAUTOPOLL;
    
    [SplusStructAttribute(13, false, false)]
    public ushort  [] VOIPINSTANCEINDEX;
    
    [SplusStructAttribute(14, false, false)]
    public ushort  VOLUMEINUSE = 0;
    
    [SplusStructAttribute(15, false, false)]
    public ushort  LASTINDEX = 0;
    
    [SplusStructAttribute(16, false, false)]
    public ushort  LASTPOLLINDEX = 0;
    
    [SplusStructAttribute(17, false, false)]
    public ushort  LASTPOLLSECONDINDEX = 0;
    
    [SplusStructAttribute(18, false, false)]
    public uint  BAUD = 0;
    
    [SplusStructAttribute(19, false, false)]
    public short  [] STATUSVOLUME;
    
    [SplusStructAttribute(20, false, false)]
    public short  [] INTERNALVOLUME;
    
    [SplusStructAttribute(21, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(22, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(23, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(24, false, false)]
    public CrestronString  LOGINNAME;
    
    [SplusStructAttribute(25, false, false)]
    public CrestronString  LOGINPASSWORD;
    
    [SplusStructAttribute(26, false, false)]
    public CrestronString  [] INSTANCETAGSNAME;
    
    [SplusStructAttribute(27, false, false)]
    public CrestronString  [] INSTANCETAGSTYPE;
    
    [SplusStructAttribute(28, false, false)]
    public ushort  [] INSTANCETAGSINDEX;
    
    [SplusStructAttribute(29, false, false)]
    public ushort  [] INSTANCETAGSINDEXSECOND;
    
    [SplusStructAttribute(30, false, false)]
    public ushort  [] INSTANCETAGSPOLL;
    
    [SplusStructAttribute(31, false, false)]
    public short  [] VOLUMEMIN;
    
    [SplusStructAttribute(32, false, false)]
    public short  [] VOLUMEMAX;
    
    [SplusStructAttribute(33, false, false)]
    public ushort  [] VOLUMESTEP;
    
    [SplusStructAttribute(34, false, false)]
    public CrestronString  TXQUEUE;
    
    [SplusStructAttribute(35, false, false)]
    public CrestronString  RXQUEUE;
    
    
    public SAUDIO( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        STATUSVOLUMEMUTE  = new ushort[ 65 ];
        STATUSWALL  = new ushort[ 65 ];
        STATUSGROUP  = new ushort[ 65 ];
        STATUSVOIPAUTOANSWER  = new ushort[ 3 ];
        STATUSVOIPDND  = new ushort[ 3 ];
        STATUSVOIPCALLSTATE  = new ushort[ 3 ];
        VOIPINSTANCEINDEX  = new ushort[ 3 ];
        INSTANCETAGSINDEX  = new ushort[ 65 ];
        INSTANCETAGSINDEXSECOND  = new ushort[ 65 ];
        INSTANCETAGSPOLL  = new ushort[ 65 ];
        VOLUMESTEP  = new ushort[ 65 ];
        STATUSVOLUME  = new short[ 65 ];
        INTERNALVOLUME  = new short[ 65 ];
        VOLUMEMIN  = new short[ 65 ];
        VOLUMEMAX  = new short[ 65 ];
        COMMANDPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMMANDAUTOPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        STATUSVOIPRECENTCALL  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            STATUSVOIPRECENTCALL [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        VOIPDIALSTRING  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            VOIPDIALSTRING [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSNAME  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSNAME [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSTYPE  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSTYPE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}

}
