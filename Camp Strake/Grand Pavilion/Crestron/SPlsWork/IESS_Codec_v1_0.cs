using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_CODEC_V1_0
{
    public class UserModuleClass_IESS_CODEC_V1_0 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput STATUSCALL_CONNECTED_FB;
        Crestron.Logos.SplusObjects.DigitalInput STATUSCALL_DISCONNECTED_FB;
        Crestron.Logos.SplusObjects.DigitalInput DIAL_SEND_END;
        Crestron.Logos.SplusObjects.DigitalInput SELFVIEWTOGGLE;
        Crestron.Logos.SplusObjects.DigitalInput AUTOANSWERTOGGLE;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_0;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_1;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_2;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_3;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_4;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_5;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_6;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_7;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_8;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_9;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_STAR;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_POUND;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_DOT;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_BACKSPACE_CLEAR;
        Crestron.Logos.SplusObjects.DigitalInput CONTENTSTOP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> CONTENTSOURCES;
        Crestron.Logos.SplusObjects.AnalogInput CONTENTSOURCEANALOG;
        Crestron.Logos.SplusObjects.DigitalOutput DIAL_SEND;
        Crestron.Logos.SplusObjects.DigitalOutput DIAL_END;
        Crestron.Logos.SplusObjects.DigitalOutput SELFVIEW_ON;
        Crestron.Logos.SplusObjects.DigitalOutput SELFVIEW_OFF;
        Crestron.Logos.SplusObjects.DigitalOutput AUTOANSWER_ON;
        Crestron.Logos.SplusObjects.DigitalOutput AUTOANSWER_OFF;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_0;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_1;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_2;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_3;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_4;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_5;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_6;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_7;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_8;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_9;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_STAR;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_POUND;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_DOT;
        Crestron.Logos.SplusObjects.DigitalOutput CONTENT_START;
        Crestron.Logos.SplusObjects.DigitalOutput CONTENT_STOP;
        Crestron.Logos.SplusObjects.AnalogOutput CONTENTSOURCE;
        Crestron.Logos.SplusObjects.StringOutput DIALSTRING;
        ushort GLBL_STATUSCALL = 0;
        ushort GLBL_SELFVIEW = 0;
        ushort GLBL_AUTOANSWER = 0;
        CrestronString GLBL_DIALSTRING;
        object DIAL_SEND_END_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 93;
                if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
                    {
                    __context__.SourceCodeLine = 94;
                    Functions.Pulse ( 20, DIAL_END ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 96;
                    Functions.Pulse ( 20, DIAL_SEND ) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object SELFVIEWTOGGLE_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 100;
            GLBL_SELFVIEW = (ushort) ( Functions.Not( GLBL_SELFVIEW ) ) ; 
            __context__.SourceCodeLine = 101;
            if ( Functions.TestForTrue  ( ( GLBL_SELFVIEW)  ) ) 
                { 
                __context__.SourceCodeLine = 103;
                SELFVIEW_OFF  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 104;
                SELFVIEW_ON  .Value = (ushort) ( 1 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 108;
                SELFVIEW_ON  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 109;
                SELFVIEW_OFF  .Value = (ushort) ( 1 ) ; 
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object AUTOANSWERTOGGLE_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 114;
        GLBL_AUTOANSWER = (ushort) ( Functions.Not( GLBL_AUTOANSWER ) ) ; 
        __context__.SourceCodeLine = 115;
        if ( Functions.TestForTrue  ( ( GLBL_AUTOANSWER)  ) ) 
            { 
            __context__.SourceCodeLine = 117;
            AUTOANSWER_OFF  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 118;
            AUTOANSWER_ON  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 122;
            AUTOANSWER_ON  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 123;
            AUTOANSWER_OFF  .Value = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CONTENTSOURCES_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVSOURCE = 0;
        
        
        __context__.SourceCodeLine = 129;
        LVSOURCE = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 130;
        CONTENTSOURCE  .Value = (ushort) ( LVSOURCE ) ; 
        __context__.SourceCodeLine = 131;
        Functions.Pulse ( 20, CONTENT_START ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CONTENTSTOP_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 135;
        CONTENTSOURCE  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 136;
        CONTENT_START  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 137;
        Functions.Pulse ( 20, CONTENT_STOP ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CONTENTSOURCEANALOG_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 141;
        CONTENTSOURCE  .Value = (ushort) ( CONTENTSOURCEANALOG  .UshortValue ) ; 
        __context__.SourceCodeLine = 142;
        if ( Functions.TestForTrue  ( ( CONTENTSOURCEANALOG  .UshortValue)  ) ) 
            {
            __context__.SourceCodeLine = 143;
            Functions.Pulse ( 20, CONTENT_START ) ; 
            }
        
        else 
            { 
            __context__.SourceCodeLine = 146;
            CONTENT_START  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 147;
            Functions.Pulse ( 20, CONTENT_STOP ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object STATUSCALL_CONNECTED_FB_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 152;
        if ( Functions.TestForTrue  ( ( STATUSCALL_CONNECTED_FB  .Value)  ) ) 
            {
            __context__.SourceCodeLine = 153;
            GLBL_STATUSCALL = (ushort) ( 1 ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 155;
            GLBL_STATUSCALL = (ushort) ( 0 ) ; 
            }
        
        __context__.SourceCodeLine = 156;
        GLBL_DIALSTRING  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 157;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object STATUSCALL_DISCONNECTED_FB_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 161;
        if ( Functions.TestForTrue  ( ( STATUSCALL_DISCONNECTED_FB  .Value)  ) ) 
            {
            __context__.SourceCodeLine = 162;
            GLBL_STATUSCALL = (ushort) ( 0 ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 164;
            GLBL_STATUSCALL = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 165;
        GLBL_DIALSTRING  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 166;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_0_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 170;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 171;
            Functions.Pulse ( 20, DTMF_0 ) ; 
            }
        
        __context__.SourceCodeLine = 172;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "0"  ) ; 
        __context__.SourceCodeLine = 173;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_1_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 177;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 178;
            Functions.Pulse ( 20, DTMF_1 ) ; 
            }
        
        __context__.SourceCodeLine = 179;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "1"  ) ; 
        __context__.SourceCodeLine = 180;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_2_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 184;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 185;
            Functions.Pulse ( 20, DTMF_2 ) ; 
            }
        
        __context__.SourceCodeLine = 186;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "2"  ) ; 
        __context__.SourceCodeLine = 187;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_3_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 191;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 192;
            Functions.Pulse ( 20, DTMF_3 ) ; 
            }
        
        __context__.SourceCodeLine = 193;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "3"  ) ; 
        __context__.SourceCodeLine = 194;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_4_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 198;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 199;
            Functions.Pulse ( 20, DTMF_4 ) ; 
            }
        
        __context__.SourceCodeLine = 200;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "4"  ) ; 
        __context__.SourceCodeLine = 201;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_5_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 205;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 206;
            Functions.Pulse ( 20, DTMF_5 ) ; 
            }
        
        __context__.SourceCodeLine = 207;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "5"  ) ; 
        __context__.SourceCodeLine = 208;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_6_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 212;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 213;
            Functions.Pulse ( 20, DTMF_6 ) ; 
            }
        
        __context__.SourceCodeLine = 214;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "6"  ) ; 
        __context__.SourceCodeLine = 215;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_7_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 219;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 220;
            Functions.Pulse ( 20, DTMF_7 ) ; 
            }
        
        __context__.SourceCodeLine = 221;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "7"  ) ; 
        __context__.SourceCodeLine = 222;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_8_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 226;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 227;
            Functions.Pulse ( 20, DTMF_8 ) ; 
            }
        
        __context__.SourceCodeLine = 228;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "8"  ) ; 
        __context__.SourceCodeLine = 229;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_9_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 233;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 234;
            Functions.Pulse ( 20, DTMF_9 ) ; 
            }
        
        __context__.SourceCodeLine = 235;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "9"  ) ; 
        __context__.SourceCodeLine = 236;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_STAR_OnPush_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 240;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 241;
            Functions.Pulse ( 20, DTMF_STAR ) ; 
            }
        
        __context__.SourceCodeLine = 242;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "*"  ) ; 
        __context__.SourceCodeLine = 243;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_POUND_OnPush_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 247;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 248;
            Functions.Pulse ( 20, DTMF_POUND ) ; 
            }
        
        __context__.SourceCodeLine = 249;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "#"  ) ; 
        __context__.SourceCodeLine = 250;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_DOT_OnPush_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 254;
        if ( Functions.TestForTrue  ( ( GLBL_STATUSCALL)  ) ) 
            {
            __context__.SourceCodeLine = 255;
            Functions.Pulse ( 20, DTMF_DOT ) ; 
            }
        
        __context__.SourceCodeLine = 256;
        GLBL_DIALSTRING  .UpdateValue ( GLBL_DIALSTRING + "."  ) ; 
        __context__.SourceCodeLine = 257;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_BACKSPACE_CLEAR_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 261;
        CreateWait ( "CLEAR" , 100 , CLEAR_Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void CLEAR_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 263;
            if ( Functions.TestForTrue  ( ( KEYPAD_BACKSPACE_CLEAR  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 265;
                GLBL_DIALSTRING  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 266;
                DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object KEYPAD_BACKSPACE_CLEAR_OnRelease_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        
        
        __context__.SourceCodeLine = 273;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DIALSTRING ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 275;
            LVSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
            __context__.SourceCodeLine = 276;
            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( GLBL_DIALSTRING ) - 1), LVSTRING )  ) ; 
            __context__.SourceCodeLine = 277;
            GLBL_DIALSTRING  .UpdateValue ( LVSTRING  ) ; 
            __context__.SourceCodeLine = 278;
            DIALSTRING  .UpdateValue ( GLBL_DIALSTRING  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GLBL_DIALSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    
    STATUSCALL_CONNECTED_FB = new Crestron.Logos.SplusObjects.DigitalInput( STATUSCALL_CONNECTED_FB__DigitalInput__, this );
    m_DigitalInputList.Add( STATUSCALL_CONNECTED_FB__DigitalInput__, STATUSCALL_CONNECTED_FB );
    
    STATUSCALL_DISCONNECTED_FB = new Crestron.Logos.SplusObjects.DigitalInput( STATUSCALL_DISCONNECTED_FB__DigitalInput__, this );
    m_DigitalInputList.Add( STATUSCALL_DISCONNECTED_FB__DigitalInput__, STATUSCALL_DISCONNECTED_FB );
    
    DIAL_SEND_END = new Crestron.Logos.SplusObjects.DigitalInput( DIAL_SEND_END__DigitalInput__, this );
    m_DigitalInputList.Add( DIAL_SEND_END__DigitalInput__, DIAL_SEND_END );
    
    SELFVIEWTOGGLE = new Crestron.Logos.SplusObjects.DigitalInput( SELFVIEWTOGGLE__DigitalInput__, this );
    m_DigitalInputList.Add( SELFVIEWTOGGLE__DigitalInput__, SELFVIEWTOGGLE );
    
    AUTOANSWERTOGGLE = new Crestron.Logos.SplusObjects.DigitalInput( AUTOANSWERTOGGLE__DigitalInput__, this );
    m_DigitalInputList.Add( AUTOANSWERTOGGLE__DigitalInput__, AUTOANSWERTOGGLE );
    
    KEYPAD_0 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_0__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_0__DigitalInput__, KEYPAD_0 );
    
    KEYPAD_1 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_1__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_1__DigitalInput__, KEYPAD_1 );
    
    KEYPAD_2 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_2__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_2__DigitalInput__, KEYPAD_2 );
    
    KEYPAD_3 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_3__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_3__DigitalInput__, KEYPAD_3 );
    
    KEYPAD_4 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_4__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_4__DigitalInput__, KEYPAD_4 );
    
    KEYPAD_5 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_5__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_5__DigitalInput__, KEYPAD_5 );
    
    KEYPAD_6 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_6__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_6__DigitalInput__, KEYPAD_6 );
    
    KEYPAD_7 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_7__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_7__DigitalInput__, KEYPAD_7 );
    
    KEYPAD_8 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_8__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_8__DigitalInput__, KEYPAD_8 );
    
    KEYPAD_9 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_9__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_9__DigitalInput__, KEYPAD_9 );
    
    KEYPAD_STAR = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_STAR__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_STAR__DigitalInput__, KEYPAD_STAR );
    
    KEYPAD_POUND = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_POUND__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_POUND__DigitalInput__, KEYPAD_POUND );
    
    KEYPAD_DOT = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_DOT__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_DOT__DigitalInput__, KEYPAD_DOT );
    
    KEYPAD_BACKSPACE_CLEAR = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_BACKSPACE_CLEAR__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_BACKSPACE_CLEAR__DigitalInput__, KEYPAD_BACKSPACE_CLEAR );
    
    CONTENTSTOP = new Crestron.Logos.SplusObjects.DigitalInput( CONTENTSTOP__DigitalInput__, this );
    m_DigitalInputList.Add( CONTENTSTOP__DigitalInput__, CONTENTSTOP );
    
    CONTENTSOURCES = new InOutArray<DigitalInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        CONTENTSOURCES[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( CONTENTSOURCES__DigitalInput__ + i, CONTENTSOURCES__DigitalInput__, this );
        m_DigitalInputList.Add( CONTENTSOURCES__DigitalInput__ + i, CONTENTSOURCES[i+1] );
    }
    
    DIAL_SEND = new Crestron.Logos.SplusObjects.DigitalOutput( DIAL_SEND__DigitalOutput__, this );
    m_DigitalOutputList.Add( DIAL_SEND__DigitalOutput__, DIAL_SEND );
    
    DIAL_END = new Crestron.Logos.SplusObjects.DigitalOutput( DIAL_END__DigitalOutput__, this );
    m_DigitalOutputList.Add( DIAL_END__DigitalOutput__, DIAL_END );
    
    SELFVIEW_ON = new Crestron.Logos.SplusObjects.DigitalOutput( SELFVIEW_ON__DigitalOutput__, this );
    m_DigitalOutputList.Add( SELFVIEW_ON__DigitalOutput__, SELFVIEW_ON );
    
    SELFVIEW_OFF = new Crestron.Logos.SplusObjects.DigitalOutput( SELFVIEW_OFF__DigitalOutput__, this );
    m_DigitalOutputList.Add( SELFVIEW_OFF__DigitalOutput__, SELFVIEW_OFF );
    
    AUTOANSWER_ON = new Crestron.Logos.SplusObjects.DigitalOutput( AUTOANSWER_ON__DigitalOutput__, this );
    m_DigitalOutputList.Add( AUTOANSWER_ON__DigitalOutput__, AUTOANSWER_ON );
    
    AUTOANSWER_OFF = new Crestron.Logos.SplusObjects.DigitalOutput( AUTOANSWER_OFF__DigitalOutput__, this );
    m_DigitalOutputList.Add( AUTOANSWER_OFF__DigitalOutput__, AUTOANSWER_OFF );
    
    DTMF_0 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_0__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_0__DigitalOutput__, DTMF_0 );
    
    DTMF_1 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_1__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_1__DigitalOutput__, DTMF_1 );
    
    DTMF_2 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_2__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_2__DigitalOutput__, DTMF_2 );
    
    DTMF_3 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_3__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_3__DigitalOutput__, DTMF_3 );
    
    DTMF_4 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_4__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_4__DigitalOutput__, DTMF_4 );
    
    DTMF_5 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_5__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_5__DigitalOutput__, DTMF_5 );
    
    DTMF_6 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_6__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_6__DigitalOutput__, DTMF_6 );
    
    DTMF_7 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_7__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_7__DigitalOutput__, DTMF_7 );
    
    DTMF_8 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_8__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_8__DigitalOutput__, DTMF_8 );
    
    DTMF_9 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_9__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_9__DigitalOutput__, DTMF_9 );
    
    DTMF_STAR = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_STAR__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_STAR__DigitalOutput__, DTMF_STAR );
    
    DTMF_POUND = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_POUND__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_POUND__DigitalOutput__, DTMF_POUND );
    
    DTMF_DOT = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_DOT__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_DOT__DigitalOutput__, DTMF_DOT );
    
    CONTENT_START = new Crestron.Logos.SplusObjects.DigitalOutput( CONTENT_START__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONTENT_START__DigitalOutput__, CONTENT_START );
    
    CONTENT_STOP = new Crestron.Logos.SplusObjects.DigitalOutput( CONTENT_STOP__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONTENT_STOP__DigitalOutput__, CONTENT_STOP );
    
    CONTENTSOURCEANALOG = new Crestron.Logos.SplusObjects.AnalogInput( CONTENTSOURCEANALOG__AnalogSerialInput__, this );
    m_AnalogInputList.Add( CONTENTSOURCEANALOG__AnalogSerialInput__, CONTENTSOURCEANALOG );
    
    CONTENTSOURCE = new Crestron.Logos.SplusObjects.AnalogOutput( CONTENTSOURCE__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONTENTSOURCE__AnalogSerialOutput__, CONTENTSOURCE );
    
    DIALSTRING = new Crestron.Logos.SplusObjects.StringOutput( DIALSTRING__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DIALSTRING__AnalogSerialOutput__, DIALSTRING );
    
    CLEAR_Callback = new WaitFunction( CLEAR_CallbackFn );
    
    DIAL_SEND_END.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIAL_SEND_END_OnPush_0, false ) );
    SELFVIEWTOGGLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( SELFVIEWTOGGLE_OnPush_1, false ) );
    AUTOANSWERTOGGLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( AUTOANSWERTOGGLE_OnPush_2, false ) );
    for( uint i = 0; i < 8; i++ )
        CONTENTSOURCES[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( CONTENTSOURCES_OnPush_3, false ) );
        
    CONTENTSTOP.OnDigitalPush.Add( new InputChangeHandlerWrapper( CONTENTSTOP_OnPush_4, false ) );
    CONTENTSOURCEANALOG.OnAnalogChange.Add( new InputChangeHandlerWrapper( CONTENTSOURCEANALOG_OnChange_5, false ) );
    STATUSCALL_CONNECTED_FB.OnDigitalChange.Add( new InputChangeHandlerWrapper( STATUSCALL_CONNECTED_FB_OnChange_6, false ) );
    STATUSCALL_DISCONNECTED_FB.OnDigitalChange.Add( new InputChangeHandlerWrapper( STATUSCALL_DISCONNECTED_FB_OnChange_7, false ) );
    KEYPAD_0.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_0_OnPush_8, false ) );
    KEYPAD_1.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_1_OnPush_9, false ) );
    KEYPAD_2.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_2_OnPush_10, false ) );
    KEYPAD_3.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_3_OnPush_11, false ) );
    KEYPAD_4.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_4_OnPush_12, false ) );
    KEYPAD_5.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_5_OnPush_13, false ) );
    KEYPAD_6.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_6_OnPush_14, false ) );
    KEYPAD_7.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_7_OnPush_15, false ) );
    KEYPAD_8.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_8_OnPush_16, false ) );
    KEYPAD_9.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_9_OnPush_17, false ) );
    KEYPAD_STAR.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_STAR_OnPush_18, false ) );
    KEYPAD_POUND.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_POUND_OnPush_19, false ) );
    KEYPAD_DOT.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_DOT_OnPush_20, false ) );
    KEYPAD_BACKSPACE_CLEAR.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_BACKSPACE_CLEAR_OnPush_21, false ) );
    KEYPAD_BACKSPACE_CLEAR.OnDigitalRelease.Add( new InputChangeHandlerWrapper( KEYPAD_BACKSPACE_CLEAR_OnRelease_22, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_CODEC_V1_0 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction CLEAR_Callback;


const uint STATUSCALL_CONNECTED_FB__DigitalInput__ = 0;
const uint STATUSCALL_DISCONNECTED_FB__DigitalInput__ = 1;
const uint DIAL_SEND_END__DigitalInput__ = 2;
const uint SELFVIEWTOGGLE__DigitalInput__ = 3;
const uint AUTOANSWERTOGGLE__DigitalInput__ = 4;
const uint KEYPAD_0__DigitalInput__ = 5;
const uint KEYPAD_1__DigitalInput__ = 6;
const uint KEYPAD_2__DigitalInput__ = 7;
const uint KEYPAD_3__DigitalInput__ = 8;
const uint KEYPAD_4__DigitalInput__ = 9;
const uint KEYPAD_5__DigitalInput__ = 10;
const uint KEYPAD_6__DigitalInput__ = 11;
const uint KEYPAD_7__DigitalInput__ = 12;
const uint KEYPAD_8__DigitalInput__ = 13;
const uint KEYPAD_9__DigitalInput__ = 14;
const uint KEYPAD_STAR__DigitalInput__ = 15;
const uint KEYPAD_POUND__DigitalInput__ = 16;
const uint KEYPAD_DOT__DigitalInput__ = 17;
const uint KEYPAD_BACKSPACE_CLEAR__DigitalInput__ = 18;
const uint CONTENTSTOP__DigitalInput__ = 19;
const uint CONTENTSOURCES__DigitalInput__ = 20;
const uint CONTENTSOURCEANALOG__AnalogSerialInput__ = 0;
const uint DIAL_SEND__DigitalOutput__ = 0;
const uint DIAL_END__DigitalOutput__ = 1;
const uint SELFVIEW_ON__DigitalOutput__ = 2;
const uint SELFVIEW_OFF__DigitalOutput__ = 3;
const uint AUTOANSWER_ON__DigitalOutput__ = 4;
const uint AUTOANSWER_OFF__DigitalOutput__ = 5;
const uint DTMF_0__DigitalOutput__ = 6;
const uint DTMF_1__DigitalOutput__ = 7;
const uint DTMF_2__DigitalOutput__ = 8;
const uint DTMF_3__DigitalOutput__ = 9;
const uint DTMF_4__DigitalOutput__ = 10;
const uint DTMF_5__DigitalOutput__ = 11;
const uint DTMF_6__DigitalOutput__ = 12;
const uint DTMF_7__DigitalOutput__ = 13;
const uint DTMF_8__DigitalOutput__ = 14;
const uint DTMF_9__DigitalOutput__ = 15;
const uint DTMF_STAR__DigitalOutput__ = 16;
const uint DTMF_POUND__DigitalOutput__ = 17;
const uint DTMF_DOT__DigitalOutput__ = 18;
const uint CONTENT_START__DigitalOutput__ = 19;
const uint CONTENT_STOP__DigitalOutput__ = 20;
const uint CONTENTSOURCE__AnalogSerialOutput__ = 0;
const uint DIALSTRING__AnalogSerialOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
