using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_ROUTE_CONVERSION_V0_2
{
    public class UserModuleClass_IESS_ROUTE_CONVERSION_V0_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> SOURCEFORDISPLAY;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> SOURCEINPUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> DISPLAYOUTPUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VIDEOOUTPUTSOURCE;
        ushort [] GVSOURCEINPUT;
        ushort [] GVDISPLAYOUTPUT;
        object SOURCEFORDISPLAY_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort LVINDEX = 0;
                
                
                __context__.SourceCodeLine = 75;
                LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 76;
                VIDEOOUTPUTSOURCE [ GVDISPLAYOUTPUT[ LVINDEX ]]  .Value = (ushort) ( GVSOURCEINPUT[ SOURCEFORDISPLAY[ LVINDEX ] .UshortValue ] ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object SOURCEINPUT_OnChange_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            ushort LVINDEX = 0;
            
            
            __context__.SourceCodeLine = 81;
            LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
            __context__.SourceCodeLine = 82;
            GVSOURCEINPUT [ LVINDEX] = (ushort) ( SOURCEINPUT[ LVINDEX ] .UshortValue ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object DISPLAYOUTPUT_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 87;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 88;
        GVDISPLAYOUTPUT [ LVINDEX] = (ushort) ( DISPLAYOUTPUT[ LVINDEX ] .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 96;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)16; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 98;
            GVSOURCEINPUT [ LVCOUNTER] = (ushort) ( LVCOUNTER ) ; 
            __context__.SourceCodeLine = 96;
            } 
        
        __context__.SourceCodeLine = 100;
        ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__2 = (ushort)16; 
        int __FN_FORSTEP_VAL__2 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
            { 
            __context__.SourceCodeLine = 102;
            GVDISPLAYOUTPUT [ LVCOUNTER] = (ushort) ( LVCOUNTER ) ; 
            __context__.SourceCodeLine = 100;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GVSOURCEINPUT  = new ushort[ 17 ];
    GVDISPLAYOUTPUT  = new ushort[ 17 ];
    
    SOURCEFORDISPLAY = new InOutArray<AnalogInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEFORDISPLAY[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( SOURCEFORDISPLAY__AnalogSerialInput__ + i, SOURCEFORDISPLAY__AnalogSerialInput__, this );
        m_AnalogInputList.Add( SOURCEFORDISPLAY__AnalogSerialInput__ + i, SOURCEFORDISPLAY[i+1] );
    }
    
    SOURCEINPUT = new InOutArray<AnalogInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEINPUT[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT__AnalogSerialInput__, this );
        m_AnalogInputList.Add( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT[i+1] );
    }
    
    DISPLAYOUTPUT = new InOutArray<AnalogInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        DISPLAYOUTPUT[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAYOUTPUT__AnalogSerialInput__ + i, DISPLAYOUTPUT__AnalogSerialInput__, this );
        m_AnalogInputList.Add( DISPLAYOUTPUT__AnalogSerialInput__ + i, DISPLAYOUTPUT[i+1] );
    }
    
    VIDEOOUTPUTSOURCE = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        VIDEOOUTPUTSOURCE[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VIDEOOUTPUTSOURCE__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VIDEOOUTPUTSOURCE__AnalogSerialOutput__ + i, VIDEOOUTPUTSOURCE[i+1] );
    }
    
    
    for( uint i = 0; i < 16; i++ )
        SOURCEFORDISPLAY[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( SOURCEFORDISPLAY_OnChange_0, false ) );
        
    for( uint i = 0; i < 16; i++ )
        SOURCEINPUT[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( SOURCEINPUT_OnChange_1, false ) );
        
    for( uint i = 0; i < 16; i++ )
        DISPLAYOUTPUT[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAYOUTPUT_OnChange_2, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_ROUTE_CONVERSION_V0_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint SOURCEFORDISPLAY__AnalogSerialInput__ = 0;
const uint SOURCEINPUT__AnalogSerialInput__ = 16;
const uint DISPLAYOUTPUT__AnalogSerialInput__ = 32;
const uint VIDEOOUTPUTSOURCE__AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
