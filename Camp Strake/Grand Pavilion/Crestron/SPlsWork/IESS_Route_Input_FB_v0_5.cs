using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_ROUTE_INPUT_FB_V0_5
{
    public class UserModuleClass_IESS_ROUTE_INPUT_FB_V0_5 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> SWITCHERFB_OUTPUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> SOURCEINPUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> OUTPUTNUM_DISPLAY;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> INPUTNAMES;
        Crestron.Logos.SplusObjects.StringOutput LASTSELECTEDSOURCENAME;
        Crestron.Logos.SplusObjects.AnalogOutput LASTSELECTEDSOURCENUM;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCENUM_DISPLAY;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SOURCENAME_DISPLAY;
        ushort [] GVSOURCEINPUT;
        ushort [] GVDISPLAYOUTPUT;
        object SWITCHERFB_OUTPUT_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort LVOUTPUTINDEX = 0;
                ushort LVSOURCEINDEX = 0;
                ushort LVCOUNTER = 0;
                ushort LVBREAK = 0;
                
                
                __context__.SourceCodeLine = 73;
                LVOUTPUTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 74;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SWITCHERFB_OUTPUT[ LVOUTPUTINDEX ] .UshortValue == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 76;
                    LASTSELECTEDSOURCENAME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 77;
                    LASTSELECTEDSOURCENUM  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 78;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)16; 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                        { 
                        __context__.SourceCodeLine = 80;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVDISPLAYOUTPUT[ LVCOUNTER ] == LVOUTPUTINDEX))  ) ) 
                            { 
                            __context__.SourceCodeLine = 82;
                            SOURCENUM_DISPLAY [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 83;
                            SOURCENAME_DISPLAY [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 78;
                        } 
                    
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 89;
                    ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__2 = (ushort)16; 
                    int __FN_FORSTEP_VAL__2 = (int)1; 
                    for ( LVSOURCEINDEX  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVSOURCEINDEX  >= __FN_FORSTART_VAL__2) && (LVSOURCEINDEX  <= __FN_FOREND_VAL__2) ) : ( (LVSOURCEINDEX  <= __FN_FORSTART_VAL__2) && (LVSOURCEINDEX  >= __FN_FOREND_VAL__2) ) ; LVSOURCEINDEX  += (ushort)__FN_FORSTEP_VAL__2) 
                        { 
                        __context__.SourceCodeLine = 91;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SWITCHERFB_OUTPUT[ LVOUTPUTINDEX ] .UshortValue == GVSOURCEINPUT[ LVSOURCEINDEX ]))  ) ) 
                            { 
                            __context__.SourceCodeLine = 93;
                            LASTSELECTEDSOURCENAME  .UpdateValue ( INPUTNAMES [ LVSOURCEINDEX ]  ) ; 
                            __context__.SourceCodeLine = 94;
                            LASTSELECTEDSOURCENUM  .Value = (ushort) ( LVSOURCEINDEX ) ; 
                            __context__.SourceCodeLine = 95;
                            ushort __FN_FORSTART_VAL__3 = (ushort) ( 0 ) ;
                            ushort __FN_FOREND_VAL__3 = (ushort)16; 
                            int __FN_FORSTEP_VAL__3 = (int)1; 
                            for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                                { 
                                __context__.SourceCodeLine = 97;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVDISPLAYOUTPUT[ LVCOUNTER ] == LVOUTPUTINDEX))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 99;
                                    SOURCENUM_DISPLAY [ LVCOUNTER]  .Value = (ushort) ( LVSOURCEINDEX ) ; 
                                    __context__.SourceCodeLine = 100;
                                    SOURCENAME_DISPLAY [ LVCOUNTER]  .UpdateValue ( INPUTNAMES [ LVSOURCEINDEX ]  ) ; 
                                    __context__.SourceCodeLine = 101;
                                    LVBREAK = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 102;
                                    break ; 
                                    } 
                                
                                __context__.SourceCodeLine = 95;
                                } 
                            
                            __context__.SourceCodeLine = 105;
                            if ( Functions.TestForTrue  ( ( LVBREAK)  ) ) 
                                {
                                __context__.SourceCodeLine = 106;
                                break ; 
                                }
                            
                            } 
                        
                        __context__.SourceCodeLine = 89;
                        } 
                    
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object SOURCEINPUT_OnChange_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            ushort LVINDEX = 0;
            
            
            __context__.SourceCodeLine = 114;
            LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
            __context__.SourceCodeLine = 115;
            GVSOURCEINPUT [ LVINDEX] = (ushort) ( SOURCEINPUT[ LVINDEX ] .UshortValue ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object OUTPUTNUM_DISPLAY_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 121;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 122;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( OUTPUTNUM_DISPLAY[ LVINDEX ] .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( OUTPUTNUM_DISPLAY[ LVINDEX ] .UshortValue <= 16 ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 123;
            GVDISPLAYOUTPUT [ LVINDEX] = (ushort) ( OUTPUTNUM_DISPLAY[ LVINDEX ] .UshortValue ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 135;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)16; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 137;
            GVDISPLAYOUTPUT [ LVCOUNTER] = (ushort) ( LVCOUNTER ) ; 
            __context__.SourceCodeLine = 135;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GVSOURCEINPUT  = new ushort[ 17 ];
    GVDISPLAYOUTPUT  = new ushort[ 17 ];
    
    SWITCHERFB_OUTPUT = new InOutArray<AnalogInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SWITCHERFB_OUTPUT[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( SWITCHERFB_OUTPUT__AnalogSerialInput__ + i, SWITCHERFB_OUTPUT__AnalogSerialInput__, this );
        m_AnalogInputList.Add( SWITCHERFB_OUTPUT__AnalogSerialInput__ + i, SWITCHERFB_OUTPUT[i+1] );
    }
    
    SOURCEINPUT = new InOutArray<AnalogInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEINPUT[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT__AnalogSerialInput__, this );
        m_AnalogInputList.Add( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT[i+1] );
    }
    
    OUTPUTNUM_DISPLAY = new InOutArray<AnalogInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        OUTPUTNUM_DISPLAY[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( OUTPUTNUM_DISPLAY__AnalogSerialInput__ + i, OUTPUTNUM_DISPLAY__AnalogSerialInput__, this );
        m_AnalogInputList.Add( OUTPUTNUM_DISPLAY__AnalogSerialInput__ + i, OUTPUTNUM_DISPLAY[i+1] );
    }
    
    LASTSELECTEDSOURCENUM = new Crestron.Logos.SplusObjects.AnalogOutput( LASTSELECTEDSOURCENUM__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( LASTSELECTEDSOURCENUM__AnalogSerialOutput__, LASTSELECTEDSOURCENUM );
    
    SOURCENUM_DISPLAY = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCENUM_DISPLAY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCENUM_DISPLAY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SOURCENUM_DISPLAY__AnalogSerialOutput__ + i, SOURCENUM_DISPLAY[i+1] );
    }
    
    INPUTNAMES = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        INPUTNAMES[i+1] = new Crestron.Logos.SplusObjects.StringInput( INPUTNAMES__AnalogSerialInput__ + i, INPUTNAMES__AnalogSerialInput__, 50, this );
        m_StringInputList.Add( INPUTNAMES__AnalogSerialInput__ + i, INPUTNAMES[i+1] );
    }
    
    LASTSELECTEDSOURCENAME = new Crestron.Logos.SplusObjects.StringOutput( LASTSELECTEDSOURCENAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( LASTSELECTEDSOURCENAME__AnalogSerialOutput__, LASTSELECTEDSOURCENAME );
    
    SOURCENAME_DISPLAY = new InOutArray<StringOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCENAME_DISPLAY[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SOURCENAME_DISPLAY__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SOURCENAME_DISPLAY__AnalogSerialOutput__ + i, SOURCENAME_DISPLAY[i+1] );
    }
    
    
    for( uint i = 0; i < 16; i++ )
        SWITCHERFB_OUTPUT[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( SWITCHERFB_OUTPUT_OnChange_0, false ) );
        
    for( uint i = 0; i < 16; i++ )
        SOURCEINPUT[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( SOURCEINPUT_OnChange_1, false ) );
        
    for( uint i = 0; i < 16; i++ )
        OUTPUTNUM_DISPLAY[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( OUTPUTNUM_DISPLAY_OnChange_2, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_ROUTE_INPUT_FB_V0_5 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint SWITCHERFB_OUTPUT__AnalogSerialInput__ = 0;
const uint SOURCEINPUT__AnalogSerialInput__ = 16;
const uint OUTPUTNUM_DISPLAY__AnalogSerialInput__ = 32;
const uint INPUTNAMES__AnalogSerialInput__ = 48;
const uint LASTSELECTEDSOURCENAME__AnalogSerialOutput__ = 0;
const uint LASTSELECTEDSOURCENUM__AnalogSerialOutput__ = 1;
const uint SOURCENUM_DISPLAY__AnalogSerialOutput__ = 2;
const uint SOURCENAME_DISPLAY__AnalogSerialOutput__ = 18;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
