using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DISPLAYS_V3_2_2
{
    public class UserModuleClass_IESS_DISPLAYS_V3_2_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_MUTE_ON;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_MUTE_OFF;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_MUTE_OVERRIDE;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_UP;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_DN;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.DigitalInput IP_CONNECT;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_ASPECT;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_VOLUME;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_POWER_TIME;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_TYPE;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_OBJ;
        Crestron.Logos.SplusObjects.AnalogInput SHARP_PROTOCOL;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput DISPLAY_ID__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput RX__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput LOGINNAME__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput LOGINPASSWORD__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        Crestron.Logos.SplusObjects.StringInput GENERIC_POWERON;
        Crestron.Logos.SplusObjects.StringInput GENERIC_POWEROFF;
        Crestron.Logos.SplusObjects.StringInput GENERIC_MUTEON;
        Crestron.Logos.SplusObjects.StringInput GENERIC_MUTEOFF;
        Crestron.Logos.SplusObjects.StringInput GENERIC_VOLUMEUP;
        Crestron.Logos.SplusObjects.StringInput GENERIC_VOLUMEDOWN;
        Crestron.Logos.SplusObjects.StringInput GENERIC_HEADER;
        Crestron.Logos.SplusObjects.StringInput GENERIC_FOOTER;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> GENERIC_INPUT;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAY_POWER_ON_FB;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAY_POWER_OFF_FB;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAY_VOLUME_MUTE_ON_FB;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAY_VOLUME_MUTE_OFF_FB;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAY_VOLUME_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAY_INPUT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAY_ASPECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAY_LAMP_HRS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX__DOLLAR__;
        SplusTcpClient TCPCLIENT;
        SplusUdpSocket UDPCLIENT;
        SLOCALDISPLAY GLBL_DISPLAY;
        SDISPLAY GLBL_DISPLAY_COMMANDS;
        CrestronString [] INPUT;
        CrestronString [] ASPECT;
        CrestronString [] INPUTPOLLDATA;
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            
            __context__.SourceCodeLine = 128;
            GLBL_DISPLAY . CTXQUEUE  .UpdateValue ( GLBL_DISPLAY . CTXQUEUE + LVSTRING + "\u000B\u000B"  ) ; 
            __context__.SourceCodeLine = 129;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , GLBL_DISPLAY.CTXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 131;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_26__" , 10 , __SPLS_TMPVAR__WAITLABEL_26___Callback ) ;
                __context__.SourceCodeLine = 129;
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_26___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            CrestronString LVTEMP;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            __context__.SourceCodeLine = 134;
            LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , GLBL_DISPLAY . CTXQUEUE )  ) ; 
            __context__.SourceCodeLine = 135;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVTEMP ) > 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 137;
                LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
                __context__.SourceCodeLine = 138;
                if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSCONNECTREQUEST)  ) ) 
                    { 
                    __context__.SourceCodeLine = 140;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue != 15))  ) ) 
                        {
                        __context__.SourceCodeLine = 141;
                        Functions.SocketSend ( TCPCLIENT , LVTEMP ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 143;
                        Functions.SocketSend ( UDPCLIENT , LVTEMP ) ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 146;
                    TX__DOLLAR__  .UpdateValue ( LVTEMP  ) ; 
                    }
                
                __context__.SourceCodeLine = 147;
                if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.DEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 148;
                    Trace( "Display TX: {0}", LVTEMP ) ; 
                    }
                
                __context__.SourceCodeLine = 149;
                GLBL_DISPLAY . LASTCOMMAND  .UpdateValue ( LVTEMP  ) ; 
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
        { 
        short LVSTATUS = 0;
        
        
        __context__.SourceCodeLine = 158;
        if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
            { 
            __context__.SourceCodeLine = 160;
            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSCONNECTED)  ) ) 
                { 
                __context__.SourceCodeLine = 162;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue != 15))  ) ) 
                    {
                    __context__.SourceCodeLine = 163;
                    LVSTATUS = (short) ( Functions.SocketDisconnectClient( TCPCLIENT ) ) ; 
                    }
                
                else 
                    { 
                    __context__.SourceCodeLine = 166;
                    LVSTATUS = (short) ( Functions.SocketUDP_Disable( UDPCLIENT ) ) ; 
                    __context__.SourceCodeLine = 167;
                    CONNECT_FB  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 168;
                    CONNECT_STATUS_FB  .Value = (ushort) ( 1 ) ; 
                    } 
                
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 172;
            if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                { 
                __context__.SourceCodeLine = 174;
                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSCONNECTED ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 176;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.IPADDRESS ) > 4 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( GLBL_DISPLAY.IPPORT > 0 ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 178;
                        if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSCONNECTREQUEST)  ) ) 
                            { 
                            __context__.SourceCodeLine = 180;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue != 15))  ) ) 
                                {
                                __context__.SourceCodeLine = 181;
                                LVSTATUS = (short) ( Functions.SocketConnectClient( TCPCLIENT , GLBL_DISPLAY.IPADDRESS , (ushort)( GLBL_DISPLAY.IPPORT ) , (ushort)( 1 ) ) ) ; 
                                }
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 184;
                                LVSTATUS = (short) ( Functions.SocketUDP_Enable( UDPCLIENT , "255.255.255.255" , (ushort)( GLBL_DISPLAY.IPPORT ) ) ) ; 
                                __context__.SourceCodeLine = 185;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTATUS == 0))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 187;
                                    CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 188;
                                    CONNECT_STATUS_FB  .Value = (ushort) ( 2 ) ; 
                                    } 
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    } 
                
                } 
            
            }
        
        
        }
        
    private CrestronString SHARPPADDING (  SplusExecutionContext __context__, ushort LVPROTOCOL ) 
        { 
        
        __context__.SourceCodeLine = 199;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 2) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 200;
            return ( "0" ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 201;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 3) ) || Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 4) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 202;
                return ( " " ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 204;
                return ( " " ) ; 
                }
            
            }
        
        
        return ""; // default return value (none specified in module)
        }
        
    private CrestronString SHARPBUILDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING , ushort LVPROTOCOL ) 
        { 
        ushort LVCOUNTER = 0;
        
        CrestronString LVCOMMAND;
        CrestronString LVCOMMANDTEMP;
        CrestronString LVCOMMANDPERM;
        CrestronString LVNUMBER;
        CrestronString LVPADDING;
        LVCOMMAND  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        LVCOMMANDTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        LVCOMMANDPERM  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        LVNUMBER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        LVPADDING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
        
        
        __context__.SourceCodeLine = 210;
        LVCOMMANDPERM  .UpdateValue ( LVINCOMING  ) ; 
        __context__.SourceCodeLine = 211;
        LVCOMMANDTEMP  .UpdateValue ( LVINCOMING  ) ; 
        __context__.SourceCodeLine = 212;
        LVCOMMAND  .UpdateValue ( Functions.Remove ( 4, LVCOMMANDTEMP )  ) ; 
        __context__.SourceCodeLine = 213;
        LVCOMMAND  .UpdateValue ( Functions.Remove ( LVCOMMAND , LVCOMMANDPERM )  ) ; 
        __context__.SourceCodeLine = 214;
        LVNUMBER  .UpdateValue ( LVCOMMANDPERM  ) ; 
        __context__.SourceCodeLine = 215;
        LVPADDING  .UpdateValue ( SHARPPADDING (  __context__ , (ushort)( LVPROTOCOL ))  ) ; 
        __context__.SourceCodeLine = 216;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 3) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 218;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( Functions.Length( LVNUMBER ) ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 220;
                LVCOMMAND  .UpdateValue ( LVCOMMAND + LVPADDING  ) ; 
                __context__.SourceCodeLine = 218;
                } 
            
            __context__.SourceCodeLine = 222;
            LVCOMMAND  .UpdateValue ( LVCOMMAND + LVNUMBER + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 224;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 2) ) || Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 4) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 226;
                LVCOMMAND  .UpdateValue ( LVCOMMAND + LVNUMBER  ) ; 
                __context__.SourceCodeLine = 227;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( Functions.Length( LVNUMBER ) ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)3; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 229;
                    LVCOMMAND  .UpdateValue ( LVCOMMAND + LVPADDING  ) ; 
                    __context__.SourceCodeLine = 227;
                    } 
                
                __context__.SourceCodeLine = 231;
                LVCOMMAND  .UpdateValue ( LVCOMMAND + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 235;
                LVPADDING  .UpdateValue ( " "  ) ; 
                __context__.SourceCodeLine = 236;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( Functions.Length( LVNUMBER ) ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)3; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 238;
                    LVCOMMAND  .UpdateValue ( LVCOMMAND + LVPADDING  ) ; 
                    __context__.SourceCodeLine = 236;
                    } 
                
                __context__.SourceCodeLine = 240;
                LVCOMMAND  .UpdateValue ( LVCOMMAND + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                } 
            
            }
        
        __context__.SourceCodeLine = 242;
        GLBL_DISPLAY . COMMANDCONFIRM = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 243;
        GLBL_DISPLAY . COMMANDACK  .UpdateValue ( LVINCOMING  ) ; 
        __context__.SourceCodeLine = 244;
        return ( LVCOMMAND ) ; 
        
        }
        
    private CrestronString BUILDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 249;
        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . STX + LVINCOMING + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
        __context__.SourceCodeLine = 250;
        return ( LVSTRING ) ; 
        
        }
        
    private CrestronString CHECKSUMBUILDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING ) 
        { 
        ushort LVCHECKSUMTOTAL = 0;
        
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 257;
        LVCHECKSUMTOTAL = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 258;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 6))  ) ) 
            { 
            __context__.SourceCodeLine = 260;
            LVCHECKSUMTOTAL = (ushort) ( Byte( LVINCOMING , (int)( 1 ) ) ) ; 
            __context__.SourceCodeLine = 261;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 2 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( LVINCOMING ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 263;
                LVCHECKSUMTOTAL = (ushort) ( (LVCHECKSUMTOTAL ^ Byte( LVINCOMING , (int)( LVCOUNTER ) )) ) ; 
                __context__.SourceCodeLine = 261;
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 266;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 7))  ) ) 
                { 
                __context__.SourceCodeLine = 268;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)Functions.Length( LVINCOMING ); 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 270;
                    LVCHECKSUMTOTAL = (ushort) ( (LVCHECKSUMTOTAL | Byte( LVINCOMING , (int)( LVCOUNTER ) )) ) ; 
                    __context__.SourceCodeLine = 268;
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 275;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)Functions.Length( LVINCOMING ); 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 277;
                    LVCHECKSUMTOTAL = (ushort) ( (LVCHECKSUMTOTAL + Byte( LVINCOMING , (int)( LVCOUNTER ) )) ) ; 
                    __context__.SourceCodeLine = 275;
                    } 
                
                __context__.SourceCodeLine = 279;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 3))  ) ) 
                    { 
                    __context__.SourceCodeLine = 281;
                    LVCHECKSUMTOTAL = (ushort) ( (256 - Functions.Low( (ushort) LVCHECKSUMTOTAL )) ) ; 
                    __context__.SourceCodeLine = 282;
                    GLBL_DISPLAY . COMMANDACK  .UpdateValue ( LVINCOMING  ) ; 
                    } 
                
                } 
            
            }
        
        __context__.SourceCodeLine = 285;
        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . STX + LVINCOMING + Functions.Chr (  (int) ( Functions.Low( (ushort) LVCHECKSUMTOTAL ) ) ) + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
        __context__.SourceCodeLine = 286;
        return ( LVSTRING ) ; 
        
        }
        
    private void SENDSTRING (  SplusExecutionContext __context__, ushort LVTYPE , CrestronString LVINCOMING ) 
        { 
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 293;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVTYPE == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (LVTYPE == 2) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 294;
            SETQUEUE (  __context__ , SHARPBUILDSTRING( __context__ , LVINCOMING , (ushort)( SHARP_PROTOCOL  .UshortValue ) )) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 295;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVTYPE >= 3 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVTYPE <= 7 ) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 296;
                SETQUEUE (  __context__ , CHECKSUMBUILDSTRING( __context__ , LVINCOMING )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 297;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 15))  ) ) 
                    { 
                    __context__.SourceCodeLine = 299;
                    LVSTRING  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 300;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)16; 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                        { 
                        __context__.SourceCodeLine = 302;
                        LVSTRING  .UpdateValue ( LVSTRING + GLBL_DISPLAY . IPADDRESS  ) ; 
                        __context__.SourceCodeLine = 300;
                        } 
                    
                    __context__.SourceCodeLine = 304;
                    LVSTRING  .UpdateValue ( LVINCOMING + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 305;
                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 308;
                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVINCOMING )) ; 
                    }
                
                }
            
            }
        
        
        }
        
    private void RUNINITIALIZATION (  SplusExecutionContext __context__, ushort LVTYPE ) 
        { 
        CrestronString LVID;
        CrestronString LVSTRING;
        LVID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
        
        
        __context__.SourceCodeLine = 313;
        GLBL_DISPLAY . NID = (ushort) ( Functions.Atoi( DISPLAY_ID__DOLLAR__ ) ) ; 
        __context__.SourceCodeLine = 314;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( DISPLAY_ID__DOLLAR__ ) < 1 ))  ) ) 
            {
            __context__.SourceCodeLine = 315;
            GLBL_DISPLAY . NID = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 316;
        
            {
            int __SPLS_TMPVAR__SWTCH_1__ = ((int)LVTYPE);
            
                { 
                if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 320;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "POWR1"  ) ; 
                    __context__.SourceCodeLine = 321;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "POWR0"  ) ; 
                    __context__.SourceCodeLine = 322;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "IAVD1"  ) ; 
                    __context__.SourceCodeLine = 323;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "IAVD2"  ) ; 
                    __context__.SourceCodeLine = 324;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "IAVD3"  ) ; 
                    __context__.SourceCodeLine = 325;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "IAVD4"  ) ; 
                    __context__.SourceCodeLine = 326;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "IAVD5"  ) ; 
                    __context__.SourceCodeLine = 327;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "WIDE8"  ) ; 
                    __context__.SourceCodeLine = 328;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "WIDE1"  ) ; 
                    __context__.SourceCodeLine = 329;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "WIDE2"  ) ; 
                    __context__.SourceCodeLine = 330;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "WIDE3"  ) ; 
                    __context__.SourceCodeLine = 331;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "WIDE10"  ) ; 
                    __context__.SourceCodeLine = 332;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "MUTE1"  ) ; 
                    __context__.SourceCodeLine = 333;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "MUTE2"  ) ; 
                    __context__.SourceCodeLine = 334;
                    GLBL_DISPLAY_COMMANDS . COMMANDSLEEP  .UpdateValue ( "RSPW1"  ) ; 
                    __context__.SourceCodeLine = 335;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "VOLM"  ) ; 
                    __context__.SourceCodeLine = 336;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "POWR????"  ) ; 
                    __context__.SourceCodeLine = 337;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "IAVD????"  ) ; 
                    __context__.SourceCodeLine = 338;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "VOLM????"  ) ; 
                    __context__.SourceCodeLine = 339;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "MUTE????"  ) ; 
                    __context__.SourceCodeLine = 340;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 341;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 342;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 343;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 347;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "POWR1"  ) ; 
                    __context__.SourceCodeLine = 348;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "POWR0"  ) ; 
                    __context__.SourceCodeLine = 349;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "INPS9"  ) ; 
                    __context__.SourceCodeLine = 350;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "INPS2"  ) ; 
                    __context__.SourceCodeLine = 351;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "INPS3"  ) ; 
                    __context__.SourceCodeLine = 352;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "INPS4"  ) ; 
                    __context__.SourceCodeLine = 353;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "INPS5"  ) ; 
                    __context__.SourceCodeLine = 354;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "WIDE8"  ) ; 
                    __context__.SourceCodeLine = 355;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "WIDE1"  ) ; 
                    __context__.SourceCodeLine = 356;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "WIDE2"  ) ; 
                    __context__.SourceCodeLine = 357;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "WIDE3"  ) ; 
                    __context__.SourceCodeLine = 358;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "WIDE10"  ) ; 
                    __context__.SourceCodeLine = 359;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "MUTE1"  ) ; 
                    __context__.SourceCodeLine = 360;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "MUTE0"  ) ; 
                    __context__.SourceCodeLine = 361;
                    GLBL_DISPLAY_COMMANDS . COMMANDSLEEP  .UpdateValue ( "RSPW1"  ) ; 
                    __context__.SourceCodeLine = 362;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "VOLM"  ) ; 
                    __context__.SourceCodeLine = 363;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "POWR????"  ) ; 
                    __context__.SourceCodeLine = 364;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "INPS????"  ) ; 
                    __context__.SourceCodeLine = 365;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "VOLM????"  ) ; 
                    __context__.SourceCodeLine = 366;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "MUTE????"  ) ; 
                    __context__.SourceCodeLine = 367;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 368;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 369;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 370;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 31 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 374;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u0008\u0022\u0000\u0000\u0000\u0002"  ) ; 
                    __context__.SourceCodeLine = 375;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "\u0008\u0022\u0000\u0000\u0000\u0001"  ) ; 
                    __context__.SourceCodeLine = 376;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "\u0008\u0022\u000A\u0000\u0005\u0000"  ) ; 
                    __context__.SourceCodeLine = 377;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "\u0008\u0022\u000A\u0000\u0005\u0001"  ) ; 
                    __context__.SourceCodeLine = 378;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "\u0008\u0022\u000A\u0000\u0005\u0002"  ) ; 
                    __context__.SourceCodeLine = 379;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "\u0008\u0022\u000A\u0000\u0005\u0003"  ) ; 
                    __context__.SourceCodeLine = 380;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 381;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0005"  ) ; 
                    __context__.SourceCodeLine = 382;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0004"  ) ; 
                    __context__.SourceCodeLine = 383;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 384;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0006"  ) ; 
                    __context__.SourceCodeLine = 385;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0003"  ) ; 
                    __context__.SourceCodeLine = 386;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "\u0008\u0022\u0002\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 387;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "\u0008\u0022\u0002\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 388;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 389;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "\u0008\u0022\u00F0\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 390;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "\u0008\u0022\u00F0\u0004\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 391;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "\u0008\u0022\u00F0\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 392;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "\u0008\u0022\u00F0\u0002\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 393;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 394;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 395;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 396;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 64 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 400;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u0011" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 401;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "\u0011" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 402;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0021"  ) ; 
                    __context__.SourceCodeLine = 403;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0023"  ) ; 
                    __context__.SourceCodeLine = 404;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0018"  ) ; 
                    __context__.SourceCodeLine = 405;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0025"  ) ; 
                    __context__.SourceCodeLine = 406;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0008"  ) ; 
                    __context__.SourceCodeLine = 407;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 408;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u000B"  ) ; 
                    __context__.SourceCodeLine = 409;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0004"  ) ; 
                    __context__.SourceCodeLine = 410;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0031"  ) ; 
                    __context__.SourceCodeLine = 411;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 412;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "\u0013" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 413;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "\u0013" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 414;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "\u0012" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001"  ) ; 
                    __context__.SourceCodeLine = 415;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "\u0011" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0000"  ) ; 
                    __context__.SourceCodeLine = 416;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0000"  ) ; 
                    __context__.SourceCodeLine = 417;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "\u0012" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0000"  ) ; 
                    __context__.SourceCodeLine = 418;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "\u0013" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0000"  ) ; 
                    __context__.SourceCodeLine = 419;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u00AA"  ) ; 
                    __context__.SourceCodeLine = 420;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 421;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 422;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 426;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u0002\u0000\u0000\u0000\u0000\u0002"  ) ; 
                    __context__.SourceCodeLine = 427;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "\u0002\u0001\u0000\u0000\u0000\u0003"  ) ; 
                    __context__.SourceCodeLine = 428;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u00A1"  ) ; 
                    __context__.SourceCodeLine = 429;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u00A2"  ) ; 
                    __context__.SourceCodeLine = 430;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u00A6"  ) ; 
                    __context__.SourceCodeLine = 431;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u0020"  ) ; 
                    __context__.SourceCodeLine = 432;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 433;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0002\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 434;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0004\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 435;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0006\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 436;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0003\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 437;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 438;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "\u0002\u0012\u0000\u0000\u0000\u0014"  ) ; 
                    __context__.SourceCodeLine = 439;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "\u0002\u0012\u0000\u0000\u0000\u0015"  ) ; 
                    __context__.SourceCodeLine = 440;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0005\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 441;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "\u0000\u00BF\u0000\u0000\u0001\u0002\u00C2"  ) ; 
                    __context__.SourceCodeLine = 442;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 443;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 444;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 445;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 446;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 32 ) ; 
                    __context__.SourceCodeLine = 447;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 448;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 449;
                    INPUTPOLLDATA [ 1 ]  .UpdateValue ( "\u0021"  ) ; 
                    __context__.SourceCodeLine = 450;
                    INPUTPOLLDATA [ 2 ]  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 451;
                    INPUTPOLLDATA [ 3 ]  .UpdateValue ( "\u0022"  ) ; 
                    __context__.SourceCodeLine = 452;
                    INPUTPOLLDATA [ 4 ]  .UpdateValue ( "\u0020"  ) ; 
                    __context__.SourceCodeLine = 453;
                    INPUTPOLLDATA [ 5 ]  .UpdateValue ( "\u0001"  ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 457;
                    LVSTRING  .UpdateValue ( "\u0030\u0041\u0030"  ) ; 
                    __context__.SourceCodeLine = 458;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( LVSTRING + "\u0041\u0030\u0043\u0002\u0043\u0032\u0030\u0033\u0044\u0036\u0030\u0030\u0030\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 459;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( LVSTRING + "\u0041\u0030\u0043\u0002\u0043\u0032\u0030\u0033\u0044\u0036\u0030\u0030\u0030\u0034\u0003"  ) ; 
                    __context__.SourceCodeLine = 460;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 461;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0032\u0003"  ) ; 
                    __context__.SourceCodeLine = 462;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0033\u0003"  ) ; 
                    __context__.SourceCodeLine = 463;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 464;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0035\u0003"  ) ; 
                    __context__.SourceCodeLine = 465;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0033\u0003"  ) ; 
                    __context__.SourceCodeLine = 466;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 467;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0032\u0003"  ) ; 
                    __context__.SourceCodeLine = 468;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0034\u0003"  ) ; 
                    __context__.SourceCodeLine = 469;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0042\u0003"  ) ; 
                    __context__.SourceCodeLine = 470;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0038\u0044\u0030\u0030\u0030\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 471;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0038\u0044\u0030\u0030\u0030\u0032\u0003"  ) ; 
                    __context__.SourceCodeLine = 472;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0032\u0030\u0030"  ) ; 
                    __context__.SourceCodeLine = 473;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( LVSTRING + "\u0041\u0030\u0036\u0002\u0030\u0031\u0044\u0036\u0003"  ) ; 
                    __context__.SourceCodeLine = 474;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( LVSTRING + "\u0043\u0030\u0036\u0002\u0030\u0030\u0036\u0030\u0003"  ) ; 
                    __context__.SourceCodeLine = 475;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( LVSTRING + "\u0043\u0030\u0036\u0002\u0030\u0030\u0036\u0032\u0003"  ) ; 
                    __context__.SourceCodeLine = 476;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( LVSTRING + "\u0043\u0030\u0036\u0002\u0030\u0030\u0038\u0044\u0003"  ) ; 
                    __context__.SourceCodeLine = 477;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u0001"  ) ; 
                    __context__.SourceCodeLine = 478;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 479;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 480;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 7) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 484;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u0017\u002E\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 485;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "\u0017\u002F\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 486;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0002"  ) ; 
                    __context__.SourceCodeLine = 487;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0003"  ) ; 
                    __context__.SourceCodeLine = 488;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0004"  ) ; 
                    __context__.SourceCodeLine = 489;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0005"  ) ; 
                    __context__.SourceCodeLine = 490;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0006"  ) ; 
                    __context__.SourceCodeLine = 491;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "\u0000\u0020\u0000\u0000\u000A"  ) ; 
                    __context__.SourceCodeLine = 492;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "\u0000\u0020\u0000\u0000\u0009"  ) ; 
                    __context__.SourceCodeLine = 493;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "\u0000\u0020\u0000\u0000\u0003"  ) ; 
                    __context__.SourceCodeLine = 494;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "\u0000\u0020\u0000\u0000\u0007"  ) ; 
                    __context__.SourceCodeLine = 495;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "\u0000\u0020\u0000\u0000\u0008"  ) ; 
                    __context__.SourceCodeLine = 496;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "\u0000\u0031\u0000\u0000\u0001"  ) ; 
                    __context__.SourceCodeLine = 497;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "\u0000\u0031\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 498;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "\u0000\u0016\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 499;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "\u0001\u0002\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 500;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "\u0000\u0001\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 501;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "\u0000\u0016\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 502;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "\u0000\u0031\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 503;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u00A9"  ) ; 
                    __context__.SourceCodeLine = 504;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u009A"  ) ; 
                    __context__.SourceCodeLine = 505;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 506;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 64 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 8) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 510;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "ka 01 01"  ) ; 
                    __context__.SourceCodeLine = 511;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "ka 01 00"  ) ; 
                    __context__.SourceCodeLine = 512;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "xb 01 90"  ) ; 
                    __context__.SourceCodeLine = 513;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "xb 01 91"  ) ; 
                    __context__.SourceCodeLine = 514;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "xb 01 92"  ) ; 
                    __context__.SourceCodeLine = 515;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "xb 01 93"  ) ; 
                    __context__.SourceCodeLine = 516;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "xb 01 40"  ) ; 
                    __context__.SourceCodeLine = 517;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "kc 01 02"  ) ; 
                    __context__.SourceCodeLine = 518;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "kc 01 01"  ) ; 
                    __context__.SourceCodeLine = 519;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "kc 01 06"  ) ; 
                    __context__.SourceCodeLine = 520;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "kc 01 09"  ) ; 
                    __context__.SourceCodeLine = 521;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "kc 01 10"  ) ; 
                    __context__.SourceCodeLine = 522;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "ke 01 00"  ) ; 
                    __context__.SourceCodeLine = 523;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "ke 01 01"  ) ; 
                    __context__.SourceCodeLine = 524;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "kf 01 "  ) ; 
                    __context__.SourceCodeLine = 525;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "ka 01 ff"  ) ; 
                    __context__.SourceCodeLine = 526;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "xb 01 ff"  ) ; 
                    __context__.SourceCodeLine = 527;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "kf 01 ff"  ) ; 
                    __context__.SourceCodeLine = 528;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "ke 01 ff"  ) ; 
                    __context__.SourceCodeLine = 529;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 530;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 531;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 532;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 64 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 9) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 536;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "PWR ON"  ) ; 
                    __context__.SourceCodeLine = 537;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "PWR OFF"  ) ; 
                    __context__.SourceCodeLine = 538;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "SOURCE 30"  ) ; 
                    __context__.SourceCodeLine = 539;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "SOURCE A1"  ) ; 
                    __context__.SourceCodeLine = 540;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "SOURCE 11"  ) ; 
                    __context__.SourceCodeLine = 541;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "SOURCE 21"  ) ; 
                    __context__.SourceCodeLine = 542;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "SOURCE 42"  ) ; 
                    __context__.SourceCodeLine = 543;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "ASPECT 20"  ) ; 
                    __context__.SourceCodeLine = 544;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "ASPECT 10"  ) ; 
                    __context__.SourceCodeLine = 545;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "ASPECT 30"  ) ; 
                    __context__.SourceCodeLine = 546;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "ASPECT 40"  ) ; 
                    __context__.SourceCodeLine = 547;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "ASPECT 50"  ) ; 
                    __context__.SourceCodeLine = 548;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "MUTE ON"  ) ; 
                    __context__.SourceCodeLine = 549;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "MUTE OFF"  ) ; 
                    __context__.SourceCodeLine = 550;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "VOL "  ) ; 
                    __context__.SourceCodeLine = 551;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "PWR?"  ) ; 
                    __context__.SourceCodeLine = 552;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "SOURCE?"  ) ; 
                    __context__.SourceCodeLine = 553;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "VOL?"  ) ; 
                    __context__.SourceCodeLine = 554;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "MUTE?"  ) ; 
                    __context__.SourceCodeLine = 555;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 556;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 557;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 558;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 30 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 10) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 563;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "PON"  ) ; 
                    __context__.SourceCodeLine = 564;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "POF"  ) ; 
                    __context__.SourceCodeLine = 565;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "IIS:HD1"  ) ; 
                    __context__.SourceCodeLine = 566;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "IIS:HD2"  ) ; 
                    __context__.SourceCodeLine = 567;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "IIS:DVI"  ) ; 
                    __context__.SourceCodeLine = 568;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "IIS:RG1"  ) ; 
                    __context__.SourceCodeLine = 569;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "IIS:RG2"  ) ; 
                    __context__.SourceCodeLine = 570;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "VSE:2"  ) ; 
                    __context__.SourceCodeLine = 571;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "VSE:1"  ) ; 
                    __context__.SourceCodeLine = 572;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "VSE:3"  ) ; 
                    __context__.SourceCodeLine = 573;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "VSE:5"  ) ; 
                    __context__.SourceCodeLine = 574;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "VSE:6"  ) ; 
                    __context__.SourceCodeLine = 575;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "AMT:1"  ) ; 
                    __context__.SourceCodeLine = 576;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "AMT:0"  ) ; 
                    __context__.SourceCodeLine = 577;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "AVL:"  ) ; 
                    __context__.SourceCodeLine = 578;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "QPW"  ) ; 
                    __context__.SourceCodeLine = 579;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "QIN"  ) ; 
                    __context__.SourceCodeLine = 580;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "QAV"  ) ; 
                    __context__.SourceCodeLine = 581;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "QAM"  ) ; 
                    __context__.SourceCodeLine = 582;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u0002"  ) ; 
                    __context__.SourceCodeLine = 583;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u0003"  ) ; 
                    __context__.SourceCodeLine = 584;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 585;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 64 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 11) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 589;
                    LVSTRING  .UpdateValue ( "\u00BE\u00EF\u0003\u0006\u0000"  ) ; 
                    __context__.SourceCodeLine = 590;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( LVSTRING + "\u00BA\u00D2\u0001\u0000\u0000\u0060\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 591;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( LVSTRING + "\u002A\u00D3\u0001\u0000\u0000\u0060\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 592;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( LVSTRING + "\u000E\u00D2\u0001\u0000\u0000\u0020\u0003\u0000"  ) ; 
                    __context__.SourceCodeLine = 593;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( LVSTRING + "\u006E\u00D6\u0001\u0000\u0000\u0020\u000D\u0000"  ) ; 
                    __context__.SourceCodeLine = 594;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( LVSTRING + "\u00FE\u00D2\u0001\u0000\u0000\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 595;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( LVSTRING + "\u00AE\u00DE\u0001\u0000\u0000\u0020\u0011\u0000"  ) ; 
                    __context__.SourceCodeLine = 596;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( LVSTRING + "\u006E\u00D3\u0001\u0000\u0000\u0020\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 597;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( LVSTRING + "\u000E\u00D1\u0001\u0000\u0008\u0020\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 598;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( LVSTRING + "\u009E\u00D0\u0001\u0000\u0008\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 599;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( LVSTRING + "\u003E\u00D6\u0001\u0000\u0008\u0020\u000A\u0000"  ) ; 
                    __context__.SourceCodeLine = 600;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( LVSTRING + "\u005E\u00D7\u0001\u0000\u0008\u0020\u0008\u0000"  ) ; 
                    __context__.SourceCodeLine = 601;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( LVSTRING + "\u009E\u00C4\u0001\u0000\u0008\u0020\u0030\u0000"  ) ; 
                    __context__.SourceCodeLine = 602;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( LVSTRING + "\u00D2\u00D6\u0001\u0000\u0002\u0020\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 603;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( LVSTRING + "\u0046\u00D3\u0001\u0000\u0002\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 604;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 605;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( LVSTRING + "\u0019\u00D3\u0002\u0000\u0000\u0060\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 606;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( LVSTRING + "\u00CD\u00D2\u0002\u0000\u0000\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 607;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( LVSTRING + "\u007A\u00C2\u0005\u0000\u0050\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 608;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( LVSTRING + "\u0075\u00D3\u0002\u0000\u0002\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 609;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 610;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 32 ) ; 
                    __context__.SourceCodeLine = 611;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 612;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 613;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLLAMPHOURS  .UpdateValue ( LVSTRING + "\u00C2\u00FF\u0002\u0000\u0090\u0010\u0000\u0000"  ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 12) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 617;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "(PWR 1)"  ) ; 
                    __context__.SourceCodeLine = 618;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "(PWR 0)"  ) ; 
                    __context__.SourceCodeLine = 619;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "(SIN3)"  ) ; 
                    __context__.SourceCodeLine = 620;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "(SIN4)"  ) ; 
                    __context__.SourceCodeLine = 621;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "(SIN6)"  ) ; 
                    __context__.SourceCodeLine = 622;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "(SIN7)"  ) ; 
                    __context__.SourceCodeLine = 623;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "(SIN1)"  ) ; 
                    __context__.SourceCodeLine = 624;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "(SZP1)"  ) ; 
                    __context__.SourceCodeLine = 625;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "(SZP2)"  ) ; 
                    __context__.SourceCodeLine = 626;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "(SZP0)"  ) ; 
                    __context__.SourceCodeLine = 627;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "(SZP3)"  ) ; 
                    __context__.SourceCodeLine = 628;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "(SZP4)"  ) ; 
                    __context__.SourceCodeLine = 629;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 630;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 631;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 632;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "(PWR ?)"  ) ; 
                    __context__.SourceCodeLine = 633;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 634;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 635;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 636;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 637;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 638;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 639;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 13) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 644;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "pow=on"  ) ; 
                    __context__.SourceCodeLine = 645;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "pow=off"  ) ; 
                    __context__.SourceCodeLine = 646;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "sour=hdmi"  ) ; 
                    __context__.SourceCodeLine = 647;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "sour=hdmi2"  ) ; 
                    __context__.SourceCodeLine = 648;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "sour=rgb"  ) ; 
                    __context__.SourceCodeLine = 649;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "sour=rgb2"  ) ; 
                    __context__.SourceCodeLine = 650;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "sour=vid"  ) ; 
                    __context__.SourceCodeLine = 651;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "asp=16:9"  ) ; 
                    __context__.SourceCodeLine = 652;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "asp=4:3"  ) ; 
                    __context__.SourceCodeLine = 653;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "asp=16:10"  ) ; 
                    __context__.SourceCodeLine = 654;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "asp=AUTO"  ) ; 
                    __context__.SourceCodeLine = 655;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "asp=WIDE"  ) ; 
                    __context__.SourceCodeLine = 656;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "mute=on"  ) ; 
                    __context__.SourceCodeLine = 657;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "mute=off"  ) ; 
                    __context__.SourceCodeLine = 658;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "vol="  ) ; 
                    __context__.SourceCodeLine = 659;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "pow=?"  ) ; 
                    __context__.SourceCodeLine = 660;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "sour=?"  ) ; 
                    __context__.SourceCodeLine = 661;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "vol=?"  ) ; 
                    __context__.SourceCodeLine = 662;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "mute=?"  ) ; 
                    __context__.SourceCodeLine = 663;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u000D*"  ) ; 
                    __context__.SourceCodeLine = 664;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "#\u000D"  ) ; 
                    __context__.SourceCodeLine = 665;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 666;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 30 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 14) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 670;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "CPOWR0000000000000001"  ) ; 
                    __context__.SourceCodeLine = 671;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "CPOWR0000000000000000"  ) ; 
                    __context__.SourceCodeLine = 672;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "CINPT0000000100000001"  ) ; 
                    __context__.SourceCodeLine = 673;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "CINPT0000000100000002"  ) ; 
                    __context__.SourceCodeLine = 674;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "CINPT0000000100000003"  ) ; 
                    __context__.SourceCodeLine = 675;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "CINPT0000000600000001"  ) ; 
                    __context__.SourceCodeLine = 676;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "CINPT0000000400000001"  ) ; 
                    __context__.SourceCodeLine = 677;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 678;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 679;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 680;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 681;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 682;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "CAMUT0000000000000001"  ) ; 
                    __context__.SourceCodeLine = 683;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "CAMUT0000000000000000"  ) ; 
                    __context__.SourceCodeLine = 684;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "CVOLU0000000000000"  ) ; 
                    __context__.SourceCodeLine = 685;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "EPOWR################"  ) ; 
                    __context__.SourceCodeLine = 686;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "EINPT################"  ) ; 
                    __context__.SourceCodeLine = 687;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "EVOLU################"  ) ; 
                    __context__.SourceCodeLine = 688;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "EAMUT################"  ) ; 
                    __context__.SourceCodeLine = 689;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "*S"  ) ; 
                    __context__.SourceCodeLine = 690;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000A"  ) ; 
                    __context__.SourceCodeLine = 691;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 692;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 15) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 696;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u00FF\u00FF\u00FF\u00FF\u00FF\u00FF"  ) ; 
                    __context__.SourceCodeLine = 697;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 698;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 699;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 700;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 701;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 702;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 703;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 704;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 705;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 706;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 707;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 708;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 709;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 710;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 711;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 712;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 713;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 714;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 715;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 716;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 717;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 718;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 16) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 722;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "DISPLAY.POWER=1"  ) ; 
                    __context__.SourceCodeLine = 723;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "DISPLAY.POWER=0"  ) ; 
                    __context__.SourceCodeLine = 724;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "SOURCE.SELECT=1"  ) ; 
                    __context__.SourceCodeLine = 725;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "SOURCE.SELECT=2"  ) ; 
                    __context__.SourceCodeLine = 726;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "SOURCE.SELECT=3"  ) ; 
                    __context__.SourceCodeLine = 727;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "SOURCE.SELECT=4"  ) ; 
                    __context__.SourceCodeLine = 728;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "SOURCE.SELECT=5"  ) ; 
                    __context__.SourceCodeLine = 729;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "ASPECT=0"  ) ; 
                    __context__.SourceCodeLine = 730;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "ASPECT=1"  ) ; 
                    __context__.SourceCodeLine = 731;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "ASPECT=3"  ) ; 
                    __context__.SourceCodeLine = 732;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "ASPECT=4"  ) ; 
                    __context__.SourceCodeLine = 733;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "ASPECT=5"  ) ; 
                    __context__.SourceCodeLine = 734;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "AUDIO.MUTE=1"  ) ; 
                    __context__.SourceCodeLine = 735;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "AUDIO.MUTE=0"  ) ; 
                    __context__.SourceCodeLine = 736;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "AUDIO.VOLUME="  ) ; 
                    __context__.SourceCodeLine = 737;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "DISPLAY.POWER?"  ) ; 
                    __context__.SourceCodeLine = 738;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "SOURCE.SELECT?"  ) ; 
                    __context__.SourceCodeLine = 739;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "AUDIO.VOLUME?"  ) ; 
                    __context__.SourceCodeLine = 740;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "AUDIO.MUTE?"  ) ; 
                    __context__.SourceCodeLine = 741;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 742;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 743;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 744;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 17) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 748;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "S0001"  ) ; 
                    __context__.SourceCodeLine = 749;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "S0002"  ) ; 
                    __context__.SourceCodeLine = 750;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "S0206"  ) ; 
                    __context__.SourceCodeLine = 751;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "S0209"  ) ; 
                    __context__.SourceCodeLine = 752;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "S0203"  ) ; 
                    __context__.SourceCodeLine = 753;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "S0207"  ) ; 
                    __context__.SourceCodeLine = 754;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "S0208"  ) ; 
                    __context__.SourceCodeLine = 755;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "S03012"  ) ; 
                    __context__.SourceCodeLine = 756;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "S03011"  ) ; 
                    __context__.SourceCodeLine = 757;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "S03017"  ) ; 
                    __context__.SourceCodeLine = 758;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "S03016"  ) ; 
                    __context__.SourceCodeLine = 759;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "S03014"  ) ; 
                    __context__.SourceCodeLine = 760;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "S0413"  ) ; 
                    __context__.SourceCodeLine = 761;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "S0413"  ) ; 
                    __context__.SourceCodeLine = 762;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "S0305"  ) ; 
                    __context__.SourceCodeLine = 763;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "G0007"  ) ; 
                    __context__.SourceCodeLine = 764;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "G0220"  ) ; 
                    __context__.SourceCodeLine = 765;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "G0305"  ) ; 
                    __context__.SourceCodeLine = 766;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 767;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "V99"  ) ; 
                    __context__.SourceCodeLine = 768;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 769;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 770;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 10 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 19) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 775;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( GENERIC_POWERON  ) ; 
                    __context__.SourceCodeLine = 776;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( GENERIC_POWEROFF  ) ; 
                    __context__.SourceCodeLine = 777;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( GENERIC_INPUT [ 1 ]  ) ; 
                    __context__.SourceCodeLine = 778;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( GENERIC_INPUT [ 2 ]  ) ; 
                    __context__.SourceCodeLine = 779;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( GENERIC_INPUT [ 3 ]  ) ; 
                    __context__.SourceCodeLine = 780;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( GENERIC_INPUT [ 4 ]  ) ; 
                    __context__.SourceCodeLine = 781;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( GENERIC_INPUT [ 5 ]  ) ; 
                    __context__.SourceCodeLine = 782;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 783;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 784;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 785;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 786;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 787;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( GENERIC_MUTEON  ) ; 
                    __context__.SourceCodeLine = 788;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( GENERIC_MUTEOFF  ) ; 
                    __context__.SourceCodeLine = 789;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 790;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 791;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 792;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 793;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 794;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( GENERIC_HEADER  ) ; 
                    __context__.SourceCodeLine = 795;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( GENERIC_FOOTER  ) ; 
                    __context__.SourceCodeLine = 796;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 797;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                } 
                
            }
            
        
        __context__.SourceCodeLine = 801;
        INPUT [ 1 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  ) ; 
        __context__.SourceCodeLine = 802;
        INPUT [ 2 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  ) ; 
        __context__.SourceCodeLine = 803;
        INPUT [ 3 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  ) ; 
        __context__.SourceCodeLine = 804;
        INPUT [ 4 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  ) ; 
        __context__.SourceCodeLine = 805;
        INPUT [ 5 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  ) ; 
        __context__.SourceCodeLine = 806;
        ASPECT [ 1 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  ) ; 
        __context__.SourceCodeLine = 807;
        ASPECT [ 2 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  ) ; 
        __context__.SourceCodeLine = 808;
        ASPECT [ 3 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  ) ; 
        __context__.SourceCodeLine = 809;
        ASPECT [ 4 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  ) ; 
        __context__.SourceCodeLine = 810;
        ASPECT [ 5 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  ) ; 
        
        }
        
    private void SETVOLUME (  SplusExecutionContext __context__, short LVVOL , ushort LVDISPLAY ) 
        { 
        CrestronString LVSTRING;
        CrestronString LVTEMP;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4, this );
        
        
        __context__.SourceCodeLine = 815;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 2) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 817;
            LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
            __context__.SourceCodeLine = 818;
            SETQUEUE (  __context__ , SHARPBUILDSTRING( __context__ , LVSTRING , (ushort)( SHARP_PROTOCOL  .UshortValue ) )) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 820;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 3) ) || Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 4) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 822;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.Chr (  (int) ( LVVOL ) )  ) ; 
                __context__.SourceCodeLine = 823;
                SETQUEUE (  __context__ , CHECKSUMBUILDSTRING( __context__ , LVSTRING )) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 825;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 10) ) || Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 14) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 827;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOL < 10 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 828;
                        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + "00" + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 829;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOL < 100 ))  ) ) 
                            {
                            __context__.SourceCodeLine = 830;
                            LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + "0" + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 831;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOL >= 100 ))  ) ) 
                                {
                                __context__.SourceCodeLine = 832;
                                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
                                }
                            
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 833;
                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 835;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 8))  ) ) 
                        { 
                        __context__.SourceCodeLine = 837;
                        MakeString ( LVTEMP , "{0:X2}", LVVOL) ; 
                        __context__.SourceCodeLine = 838;
                        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + LVTEMP  ) ; 
                        __context__.SourceCodeLine = 839;
                        SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 841;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 5))  ) ) 
                            { 
                            __context__.SourceCodeLine = 843;
                            MakeString ( LVTEMP , "{0:X2}", LVVOL) ; 
                            __context__.SourceCodeLine = 844;
                            LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + LVTEMP + "\u0000"  ) ; 
                            __context__.SourceCodeLine = 845;
                            SETQUEUE (  __context__ , CHECKSUMBUILDSTRING( __context__ , LVSTRING )) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 847;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 6))  ) ) 
                                { 
                                __context__.SourceCodeLine = 849;
                                MakeString ( LVTEMP , "{0:X2}", LVVOL) ; 
                                __context__.SourceCodeLine = 850;
                                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.Mid ( LVTEMP ,  (int) ( 1 ) ,  (int) ( 1 ) ) + Functions.Mid ( LVTEMP ,  (int) ( 2 ) ,  (int) ( 1 ) ) + "\u0003"  ) ; 
                                __context__.SourceCodeLine = 851;
                                SETQUEUE (  __context__ , CHECKSUMBUILDSTRING( __context__ , LVSTRING )) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 853;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 9))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 855;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOL < 10 ))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 856;
                                        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + "0" + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
                                        }
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 858;
                                        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
                                        }
                                    
                                    __context__.SourceCodeLine = 859;
                                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 861;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 13))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 863;
                                        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
                                        __context__.SourceCodeLine = 864;
                                        SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        }
        
    private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
        { 
        CrestronString LVSTRING;
        CrestronString LVRECEIVETEST;
        CrestronString LVTRASH;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVRECEIVETEST  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 872;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 923 ))  ) ) 
            {
            __context__.SourceCodeLine = 873;
            GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
            }
        
        __context__.SourceCodeLine = 874;
        if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.DEBUG)  ) ) 
            {
            __context__.SourceCodeLine = 875;
            Trace( "Display RX: {0}", GLBL_DISPLAY . CRXQUEUE ) ; 
            }
        
        __context__.SourceCodeLine = 877;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 879;
            if ( Functions.TestForTrue  ( ( Functions.Find( "Login:" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 881;
                GLBL_DISPLAY . IPLOGIN = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 882;
                LVSTRING  .UpdateValue ( LOGINNAME__DOLLAR__ + "\u000D"  ) ; 
                __context__.SourceCodeLine = 883;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 885;
                if ( Functions.TestForTrue  ( ( Functions.Find( "Password:" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 887;
                    LVSTRING  .UpdateValue ( LOGINPASSWORD__DOLLAR__ + "\u000D"  ) ; 
                    __context__.SourceCodeLine = 888;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 890;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 892;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 894;
                    LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                    __context__.SourceCodeLine = 895;
                    LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 897;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2))  ) ) 
                        { 
                        __context__.SourceCodeLine = 899;
                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 900;
                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 2), LVSTRING )  ) ; 
                        } 
                    
                    }
                
                __context__.SourceCodeLine = 903;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "0"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 906;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                        { 
                        __context__.SourceCodeLine = 908;
                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 909;
                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 910;
                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 913;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                            { 
                            __context__.SourceCodeLine = 915;
                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 916;
                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 917;
                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 920;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                { 
                                __context__.SourceCodeLine = 922;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) >= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) <= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 924;
                                    GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                    __context__.SourceCodeLine = 925;
                                    DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
                                    } 
                                
                                } 
                            
                            }
                        
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 930;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "1"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 933;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                            { 
                            __context__.SourceCodeLine = 935;
                            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                            __context__.SourceCodeLine = 936;
                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 937;
                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 938;
                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 941;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                                { 
                                __context__.SourceCodeLine = 943;
                                GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                __context__.SourceCodeLine = 944;
                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 945;
                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 946;
                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 949;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 951;
                                    GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                    __context__.SourceCodeLine = 952;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) >= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) <= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 954;
                                        GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                        __context__.SourceCodeLine = 955;
                                        DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 959;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLINPUT))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 961;
                                        GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                        __context__.SourceCodeLine = 962;
                                        DISPLAY_INPUT_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        __context__.SourceCodeLine = 963;
                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 966;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "OK" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 968;
                            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSCONNECTREQUEST)  ) ) 
                                {
                                __context__.SourceCodeLine = 969;
                                GLBL_DISPLAY . IPLOGIN = (ushort) ( 1 ) ; 
                                }
                            
                            __context__.SourceCodeLine = 970;
                            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.COMMANDCONFIRM)  ) ) 
                                { 
                                __context__.SourceCodeLine = 972;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWERON))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 974;
                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 975;
                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 976;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 978;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 980;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 981;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 982;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 984;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEON))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 986;
                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 987;
                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 988;
                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 990;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEOFF))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 992;
                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 993;
                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 994;
                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 998;
                                                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                                ushort __FN_FOREND_VAL__1 = (ushort)5; 
                                                int __FN_FORSTEP_VAL__1 = (int)1; 
                                                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                                    { 
                                                    __context__.SourceCodeLine = 1000;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == INPUT[ LVCOUNTER ]))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1002;
                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                                        __context__.SourceCodeLine = 1003;
                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                                        __context__.SourceCodeLine = 1004;
                                                        break ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 998;
                                                    } 
                                                
                                                } 
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                __context__.SourceCodeLine = 1008;
                                GLBL_DISPLAY . COMMANDACK  .UpdateValue ( ""  ) ; 
                                } 
                            
                            __context__.SourceCodeLine = 1010;
                            GLBL_DISPLAY . COMMANDCONFIRM = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1012;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "ERR" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1014;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1016;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                    } 
                                
                                __context__.SourceCodeLine = 1018;
                                if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.COMMANDCONFIRM)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1021;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWERON))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1023;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1025;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1027;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 1030;
                                GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                __context__.SourceCodeLine = 1031;
                                GLBL_DISPLAY . COMMANDACK  .UpdateValue ( ""  ) ; 
                                __context__.SourceCodeLine = 1032;
                                GLBL_DISPLAY . COMMANDCONFIRM = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 1037;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1039;
                                    GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                    __context__.SourceCodeLine = 1040;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) >= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) <= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1042;
                                        GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                        __context__.SourceCodeLine = 1043;
                                        DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1047;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLINPUT))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1049;
                                        GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                        __context__.SourceCodeLine = 1050;
                                        DISPLAY_INPUT_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        __context__.SourceCodeLine = 1051;
                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 890;
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1057;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 3))  ) ) 
                { 
                __context__.SourceCodeLine = 1059;
                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0000\u0000\u0000\u00F1\u0005\u0000\u0000\u000E" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1061;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0000\u0000\u0000\u00F1\u0005\u0000\u0000\u000E" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                    __context__.SourceCodeLine = 1062;
                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 1063;
                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 1064;
                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1066;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0000\u0000\u0000\u00F1\u0005\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1068;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0000\u0000\u0000\u00F1\u0005\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1069;
                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1070;
                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1071;
                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1073;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0002\u0000\u0000\u00F1\u0001" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1075;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0002\u0000\u0000\u00F1\u0001" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                            __context__.SourceCodeLine = 1076;
                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1077;
                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 1078;
                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1080;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0002\u0000\u0000\u00F1\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1082;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0002\u0000\u0000\u00F1\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                __context__.SourceCodeLine = 1083;
                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 1084;
                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 1085;
                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1087;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1089;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                    __context__.SourceCodeLine = 1090;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWERON))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1092;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1093;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1094;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1096;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1098;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1099;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1100;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1103;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.CRXQUEUE == "\u0000"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1105;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                        __context__.SourceCodeLine = 1106;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1107;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1108;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 1110;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 1111;
                    GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1114;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 4))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1116;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0011\u0000"  ) ; 
                    __context__.SourceCodeLine = 1117;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1119;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1120;
                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1121;
                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1122;
                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1124;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0011\u0001"  ) ; 
                    __context__.SourceCodeLine = 1125;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1127;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1128;
                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1129;
                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1130;
                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1132;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0012"  ) ; 
                    __context__.SourceCodeLine = 1133;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1135;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1136;
                        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1138;
                            GLBL_DISPLAY . STATUSVOLUME = (short) ( Byte( GLBL_DISPLAY.CRXQUEUE , (int)( 1 ) ) ) ; 
                            __context__.SourceCodeLine = 1139;
                            DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1142;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0013\u0000"  ) ; 
                    __context__.SourceCodeLine = 1143;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1145;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1146;
                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1147;
                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1148;
                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1150;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0013\u0001"  ) ; 
                    __context__.SourceCodeLine = 1151;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1153;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1154;
                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1155;
                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1156;
                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1158;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0014"  ) ; 
                    __context__.SourceCodeLine = 1159;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1161;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1162;
                        ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                        ushort __FN_FOREND_VAL__2 = (ushort)5; 
                        int __FN_FORSTEP_VAL__2 = (int)1; 
                        for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                            { 
                            __context__.SourceCodeLine = 1164;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( GLBL_DISPLAY.CRXQUEUE , (int)( 1 ) ) == Functions.Right( INPUT[ LVCOUNTER ] , (int)( 1 ) )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1166;
                                DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                __context__.SourceCodeLine = 1167;
                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                } 
                            
                            __context__.SourceCodeLine = 1162;
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1171;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 1172;
                        GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1175;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 5))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1177;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0020\u00BF" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1179;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0010\u0002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1181;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0010\u0002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                __context__.SourceCodeLine = 1182;
                                LVRECEIVETEST  .UpdateValue ( Functions.Mid ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 1 ) ,  (int) ( 1 ) )  ) ; 
                                __context__.SourceCodeLine = 1183;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVRECEIVETEST == "\u0000") ) || Functions.TestForTrue ( Functions.BoolToInt (LVRECEIVETEST == "\u000F") )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1185;
                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1186;
                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1187;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1189;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVRECEIVETEST == "\u0004"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1191;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1192;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1193;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1195;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVRECEIVETEST == "\u0005"))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1197;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1198;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1199;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 2 ) ; 
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                __context__.SourceCodeLine = 1201;
                                LVRECEIVETEST  .UpdateValue ( Functions.Mid ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 4 ) ,  (int) ( 1 ) )  ) ; 
                                __context__.SourceCodeLine = 1202;
                                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                                ushort __FN_FOREND_VAL__3 = (ushort)5; 
                                int __FN_FORSTEP_VAL__3 = (int)1; 
                                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                                    { 
                                    __context__.SourceCodeLine = 1204;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (INPUTPOLLDATA[ LVCOUNTER ] == LVRECEIVETEST))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1206;
                                        DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                        __context__.SourceCodeLine = 1207;
                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                        } 
                                    
                                    __context__.SourceCodeLine = 1202;
                                    } 
                                
                                __context__.SourceCodeLine = 1210;
                                LVRECEIVETEST  .UpdateValue ( Functions.Mid ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 7 ) ,  (int) ( 1 ) )  ) ; 
                                __context__.SourceCodeLine = 1211;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVRECEIVETEST == "\u0000"))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1213;
                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1214;
                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1215;
                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1217;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVRECEIVETEST == "\u0001"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1219;
                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1220;
                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1221;
                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 1225;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                            {
                            __context__.SourceCodeLine = 1226;
                            GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1229;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 6))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1231;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "0200D60000040001" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1233;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "0200D60000040001" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                __context__.SourceCodeLine = 1234;
                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 1235;
                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 1236;
                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1238;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "0200D60000040002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1240;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "0200D60000040002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                    __context__.SourceCodeLine = 1241;
                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1242;
                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1243;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1245;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "0200D60000040003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1247;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "0200D60000040003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                        __context__.SourceCodeLine = 1248;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1249;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1250;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1252;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "0200D60000040004" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1254;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "0200D60000040004" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                            __context__.SourceCodeLine = 1255;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1256;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1257;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1259;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "3D60001" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1261;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "3D60001" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                __context__.SourceCodeLine = 1262;
                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1263;
                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1264;
                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1266;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "3D60002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1268;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "3D60002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                    __context__.SourceCodeLine = 1269;
                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1270;
                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1271;
                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1273;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "3D60003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1275;
                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "3D60003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                        __context__.SourceCodeLine = 1276;
                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1277;
                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1278;
                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1280;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "3D60004" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1282;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "3D60004" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                            __context__.SourceCodeLine = 1283;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1284;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1285;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1287;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "00008D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1289;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "00008D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1292;
                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1294;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "000060" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1296;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "000060" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                    __context__.SourceCodeLine = 1298;
                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1300;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "000062" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1302;
                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "000062" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1303;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1305;
                                                                            GLBL_DISPLAY . STATUSVOLUME = (short) ( Byte( LVSTRING , (int)( 1 ) ) ) ; 
                                                                            __context__.SourceCodeLine = 1306;
                                                                            DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
                                                                            } 
                                                                        
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            __context__.SourceCodeLine = 1309;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                                {
                                __context__.SourceCodeLine = 1310;
                                GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1313;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 7))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1315;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1317;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                    __context__.SourceCodeLine = 1318;
                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1319;
                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1320;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1322;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0004" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1324;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0004" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                        __context__.SourceCodeLine = 1325;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1326;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1327;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1329;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0005" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1331;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0005" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                            __context__.SourceCodeLine = 1332;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1333;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1334;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1336;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0006" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1338;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0006" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                __context__.SourceCodeLine = 1339;
                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1340;
                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1341;
                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1343;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0007" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1345;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0007" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                    __context__.SourceCodeLine = 1346;
                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1347;
                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1348;
                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1350;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0008" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1352;
                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0008" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                        __context__.SourceCodeLine = 1353;
                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1354;
                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1355;
                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1357;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0001" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1359;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0001" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                            __context__.SourceCodeLine = 1360;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1361;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1362;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1364;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1366;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1367;
                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1368;
                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1369;
                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1371;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1373;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                    __context__.SourceCodeLine = 1374;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1375;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1376;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1378;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0031\u0002\u0000\u0001\u0033" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1380;
                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0031\u0002\u0000\u0001\u0033" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1381;
                                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1382;
                                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1383;
                                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1385;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0031\u0002\u0000\u0001\u0033" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1387;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0031\u0002\u0000\u0000\u0033" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                            __context__.SourceCodeLine = 1388;
                                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1389;
                                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1390;
                                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1392;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0001\u0002\u0000\u0005" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1394;
                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0001\u0002\u0000\u0005" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                __context__.SourceCodeLine = 1395;
                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                __context__.SourceCodeLine = 1396;
                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1398;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0001\u0002\u0000\u0004" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1400;
                                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0001\u0002\u0000\u0004" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                    __context__.SourceCodeLine = 1401;
                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                                    __context__.SourceCodeLine = 1402;
                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 1404;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0001\u0002\u0000\u0003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 1406;
                                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0001\u0002\u0000\u0003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                        __context__.SourceCodeLine = 1407;
                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                                        __context__.SourceCodeLine = 1408;
                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 1410;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0001\u0002\u0000\u0002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 1412;
                                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0001\u0002\u0000\u0002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                            __context__.SourceCodeLine = 1413;
                                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                                            __context__.SourceCodeLine = 1414;
                                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                                            } 
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 1416;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0016\u0002\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 1418;
                                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0016\u0002\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                                __context__.SourceCodeLine = 1419;
                                                                                                LVSTRING  .UpdateValue ( ""  ) ; 
                                                                                                __context__.SourceCodeLine = 1420;
                                                                                                LVSTRING  .UpdateValue ( Functions.Left ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 1 ) )  ) ; 
                                                                                                __context__.SourceCodeLine = 1421;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 1423;
                                                                                                    GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.HextoI( LVSTRING ) ) ; 
                                                                                                    __context__.SourceCodeLine = 1424;
                                                                                                    DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
                                                                                                    } 
                                                                                                
                                                                                                } 
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                __context__.SourceCodeLine = 1427;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 1428;
                                    GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1431;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 8))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1433;
                                    while ( Functions.TestForTrue  ( ( Functions.Find( "x" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1435;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( "x" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                        __context__.SourceCodeLine = 1436;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 1437;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "a 01 OK00" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1439;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1440;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1441;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1443;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "a 01 OK01" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1445;
                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1446;
                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1447;
                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1449;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "e 01 OK01" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1451;
                                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1452;
                                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1453;
                                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1455;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "e 01 OK00" , LVSTRING ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1457;
                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1458;
                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1459;
                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1461;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "f 01 OK" , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1463;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "f 01 OK" , LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1464;
                                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1466;
                                                            GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.HextoI( LVSTRING ) ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1468;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "b 01 OK" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1470;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "b 01 OK" , LVSTRING )  ) ; 
                                                                __context__.SourceCodeLine = 1471;
                                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                                __context__.SourceCodeLine = 1472;
                                                                ushort __FN_FORSTART_VAL__4 = (ushort) ( 1 ) ;
                                                                ushort __FN_FOREND_VAL__4 = (ushort)5; 
                                                                int __FN_FORSTEP_VAL__4 = (int)1; 
                                                                for ( LVCOUNTER  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__4) && (LVCOUNTER  <= __FN_FOREND_VAL__4) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__4) && (LVCOUNTER  >= __FN_FOREND_VAL__4) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__4) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1474;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == Functions.Right( INPUT[ LVCOUNTER ] , (int)( 2 ) )))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1476;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                                                        __context__.SourceCodeLine = 1477;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                                                        } 
                                                                    
                                                                    __context__.SourceCodeLine = 1472;
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        __context__.SourceCodeLine = 1433;
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1484;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 9))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1486;
                                        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1488;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                            __context__.SourceCodeLine = 1489;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 1490;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "ESC/VP.net" , LVSTRING ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 1491;
                                                GLBL_DISPLAY . IPLOGIN = (ushort) ( 1 ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 1492;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "PWR" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1494;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "PWR" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 1495;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ON" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "01" , LVSTRING ) )) ) ) || Functions.TestForTrue ( Functions.Find( "02" , LVSTRING ) )) ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1497;
                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1498;
                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1499;
                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1501;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "OFF" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "00" , LVSTRING ) )) ) ) || Functions.TestForTrue ( Functions.Find( "03" , LVSTRING ) )) ) ) || Functions.TestForTrue ( Functions.Find( "04" , LVSTRING ) )) ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1503;
                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1504;
                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1505;
                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    }
                                                
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1508;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "SOURCE" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1510;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "SOURCE" , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 1511;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "A0" , LVSTRING ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1513;
                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1514;
                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1516;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "A1" , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1518;
                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                            __context__.SourceCodeLine = 1519;
                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1521;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "11" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1523;
                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                __context__.SourceCodeLine = 1524;
                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1526;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "21" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1528;
                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                    __context__.SourceCodeLine = 1529;
                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1531;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "42" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1533;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                        __context__.SourceCodeLine = 1534;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1537;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOL" , LVSTRING ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1539;
                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "VOL" , LVSTRING )  ) ; 
                                                        __context__.SourceCodeLine = 1541;
                                                        GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1543;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "MUTE" , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1545;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "VOL" , LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1546;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "ON" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1548;
                                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1549;
                                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1550;
                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1552;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "OFF" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1554;
                                                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1555;
                                                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1556;
                                                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            } 
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            __context__.SourceCodeLine = 1486;
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1562;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 10))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1564;
                                            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "\u0003" , GLBL_DISPLAY.CRXQUEUE ) ) || Functions.TestForTrue ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ) )) ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1566;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 1567;
                                                    LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1568;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 1569;
                                                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u0003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                        }
                                                    
                                                    }
                                                
                                                __context__.SourceCodeLine = 1570;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 1571;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0002" , LVSTRING ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 1572;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0002" , LVSTRING )  ) ; 
                                                    }
                                                
                                                __context__.SourceCodeLine = 1574;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "000"))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1577;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1579;
                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1580;
                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1581;
                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1584;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1586;
                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1587;
                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1588;
                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1591;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1593;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) >= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) <= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) )) ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1596;
                                                                    GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1601;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "001"))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1604;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1606;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1607;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1608;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1611;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1613;
                                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1614;
                                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1615;
                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1618;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1620;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) >= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) <= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) )) ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1623;
                                                                        GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1627;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "POF" , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1629;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1630;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1631;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1633;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "PON" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1635;
                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1636;
                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1637;
                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                { 
                                                                __context__.SourceCodeLine = 1642;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1644;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) >= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) <= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) )) ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1647;
                                                                        GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1652;
                                                                    ushort __FN_FORSTART_VAL__5 = (ushort) ( 1 ) ;
                                                                    ushort __FN_FOREND_VAL__5 = (ushort)5; 
                                                                    int __FN_FORSTEP_VAL__5 = (int)1; 
                                                                    for ( LVCOUNTER  = __FN_FORSTART_VAL__5; (__FN_FORSTEP_VAL__5 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__5) && (LVCOUNTER  <= __FN_FOREND_VAL__5) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__5) && (LVCOUNTER  >= __FN_FOREND_VAL__5) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__5) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1654;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == Functions.Right( INPUT[ LVCOUNTER ] , (int)( 3 ) )))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1656;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                                                            __context__.SourceCodeLine = 1657;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                                                            } 
                                                                        
                                                                        __context__.SourceCodeLine = 1652;
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                __context__.SourceCodeLine = 1564;
                                                } 
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1665;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 11))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1667;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u001D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1669;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1671;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0000\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1673;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0000\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                            __context__.SourceCodeLine = 1674;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1675;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1676;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1678;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0002\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1680;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0002\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1681;
                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1682;
                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1683;
                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1685;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0001\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1687;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0001\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                    __context__.SourceCodeLine = 1688;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1689;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1690;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1693;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLINPUT))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1695;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0000\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1697;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0000\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1698;
                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                __context__.SourceCodeLine = 1699;
                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1701;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0001\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1703;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0001\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                    __context__.SourceCodeLine = 1704;
                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                    __context__.SourceCodeLine = 1705;
                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1707;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1709;
                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1710;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1711;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1713;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1715;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u000D\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                            __context__.SourceCodeLine = 1716;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                            __context__.SourceCodeLine = 1717;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1719;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1721;
                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u000B\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1723;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0011\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1725;
                                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0011\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                    __context__.SourceCodeLine = 1726;
                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                    __context__.SourceCodeLine = 1727;
                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                    } 
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1730;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                                                { 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1733;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1735;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0000\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1737;
                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0000\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1738;
                                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1739;
                                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1740;
                                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1742;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0001\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1744;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0001\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                            __context__.SourceCodeLine = 1745;
                                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1746;
                                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1747;
                                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        }
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1750;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLLAMPHOURS))  ) ) 
                                                                        { 
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1754;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 1755;
                                                    GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                                                    }
                                                
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1758;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 12))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1760;
                                                    while ( Functions.TestForTrue  ( ( Functions.Find( ")" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1762;
                                                        LVSTRING  .UpdateValue ( Functions.Remove ( ")" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                        __context__.SourceCodeLine = 1763;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "(SST!003 " , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1765;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "(SST!003 " , LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1766;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI 1" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1768;
                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1769;
                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1771;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI 2" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1773;
                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                    __context__.SourceCodeLine = 1774;
                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1776;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "DisplayPort" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1778;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                        __context__.SourceCodeLine = 1779;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1781;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "Component" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1783;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                            __context__.SourceCodeLine = 1784;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1786;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "VGA" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1788;
                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                __context__.SourceCodeLine = 1789;
                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                } 
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1792;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "(PWR!0)" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1794;
                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1795;
                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1796;
                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1798;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "(PWR!10)" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1800;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1801;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1802;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1804;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "(0-1,0)" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1806;
                                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1807;
                                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1808;
                                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1810;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "(PWR!1)" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1812;
                                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1813;
                                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1814;
                                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        __context__.SourceCodeLine = 1760;
                                                        } 
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1819;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 13))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1821;
                                                        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1823;
                                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                            __context__.SourceCodeLine = 1824;
                                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1825;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*pow=" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1827;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "POW=ON" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1829;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1830;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1831;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1833;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "POW=OFF" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1835;
                                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1836;
                                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1837;
                                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1840;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*sour=" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1842;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI2" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1844;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                        __context__.SourceCodeLine = 1845;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1847;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1849;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1850;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1852;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "RGB2" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1854;
                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                __context__.SourceCodeLine = 1855;
                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1857;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "RGB" , LVSTRING ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1859;
                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                                    __context__.SourceCodeLine = 1860;
                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 1862;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VID" , LVSTRING ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 1864;
                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                        __context__.SourceCodeLine = 1865;
                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 1867;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "NETWORK" , LVSTRING ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 1869;
                                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 6 ) ; 
                                                                                            __context__.SourceCodeLine = 1870;
                                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 6 ) ; 
                                                                                            } 
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1873;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*mute=" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1875;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "MUTE=ON" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1877;
                                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1878;
                                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1879;
                                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1881;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "MUTE=OFF" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1883;
                                                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                __context__.SourceCodeLine = 1884;
                                                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                __context__.SourceCodeLine = 1885;
                                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                                } 
                                                                            
                                                                            }
                                                                        
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1888;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*vol=" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1890;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "*vol=" , LVSTRING )  ) ; 
                                                                            __context__.SourceCodeLine = 1891;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*VOL=" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1893;
                                                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                                                __context__.SourceCodeLine = 1895;
                                                                                GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                } 
                                                                            
                                                                            } 
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 1821;
                                                            } 
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1901;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 14))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1903;
                                                            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000A" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1905;
                                                                LVSTRING  .UpdateValue ( Functions.Remove ( "\u000A" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1906;
                                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                                __context__.SourceCodeLine = 1907;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*SNPOWR0000000000000001" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1909;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1910;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1911;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1913;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*SNPOWR0000000000000000" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1915;
                                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1916;
                                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1917;
                                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1919;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000100000001" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1921;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1922;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1924;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000100000002" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1926;
                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                                __context__.SourceCodeLine = 1927;
                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1929;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000100000003" , LVSTRING ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1931;
                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                                    __context__.SourceCodeLine = 1932;
                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 1934;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000600000001" , LVSTRING ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 1936;
                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                        __context__.SourceCodeLine = 1937;
                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 1939;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000400000001" , LVSTRING ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 1941;
                                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                            __context__.SourceCodeLine = 1942;
                                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                            } 
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 1944;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*SNVOLU" , LVSTRING ))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 1946;
                                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "*SNVOLU" , LVSTRING )  ) ; 
                                                                                                __context__.SourceCodeLine = 1947;
                                                                                                GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                __context__.SourceCodeLine = 1948;
                                                                                                DISPLAY_VOLUME_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                } 
                                                                                            
                                                                                            else 
                                                                                                {
                                                                                                __context__.SourceCodeLine = 1950;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*SNAMUT0000000000000001" , LVSTRING ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 1952;
                                                                                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                    __context__.SourceCodeLine = 1953;
                                                                                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                                    __context__.SourceCodeLine = 1954;
                                                                                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                                                    } 
                                                                                                
                                                                                                else 
                                                                                                    {
                                                                                                    __context__.SourceCodeLine = 1956;
                                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*SNAMUT0000000000000000" , LVSTRING ))  ) ) 
                                                                                                        { 
                                                                                                        __context__.SourceCodeLine = 1958;
                                                                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                                        __context__.SourceCodeLine = 1959;
                                                                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                        __context__.SourceCodeLine = 1960;
                                                                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                                                        } 
                                                                                                    
                                                                                                    }
                                                                                                
                                                                                                }
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                __context__.SourceCodeLine = 1903;
                                                                } 
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1965;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 15))  ) ) 
                                                                { 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1969;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 16))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1971;
                                                                    while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1973;
                                                                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1974;
                                                                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                                        __context__.SourceCodeLine = 1975;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "DISPLAY.POWER:OFF" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "DISPLAY.POWER:0" , LVSTRING ) )) ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1977;
                                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1978;
                                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1979;
                                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1981;
                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "DISPLAY.POWER:ON" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "DISPLAY.POWER:1" , LVSTRING ) )) ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1983;
                                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                __context__.SourceCodeLine = 1984;
                                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                __context__.SourceCodeLine = 1985;
                                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1987;
                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "AUDIO.MUTE:OFF" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "AUDIO.MUTE:0" , LVSTRING ) )) ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1989;
                                                                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                    __context__.SourceCodeLine = 1990;
                                                                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                    __context__.SourceCodeLine = 1991;
                                                                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 1993;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "AUDIO.MUTE:ON" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "AUDIO.MUTE:1" , LVSTRING ) )) ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 1995;
                                                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                        __context__.SourceCodeLine = 1996;
                                                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                        __context__.SourceCodeLine = 1997;
                                                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 1999;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "AUDIO.VOLUME:" , LVSTRING ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 2001;
                                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "AUDIO.VOLUME:" , LVSTRING )  ) ; 
                                                                                            __context__.SourceCodeLine = 2002;
                                                                                            DISPLAY_VOLUME_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                            __context__.SourceCodeLine = 2003;
                                                                                            GLBL_DISPLAY . STATUSVOLUME = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                            } 
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 2005;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "SOURCE.SELECT:" , LVSTRING ))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 2007;
                                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "SOURCE.SELECT:" , LVSTRING )  ) ; 
                                                                                                __context__.SourceCodeLine = 2008;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI." , LVSTRING ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 2010;
                                                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "HDMI." , LVSTRING )  ) ; 
                                                                                                    __context__.SourceCodeLine = 2011;
                                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                    __context__.SourceCodeLine = 2012;
                                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                    } 
                                                                                                
                                                                                                __context__.SourceCodeLine = 2014;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "DP" , LVSTRING ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 2016;
                                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                                    __context__.SourceCodeLine = 2017;
                                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                                    } 
                                                                                                
                                                                                                else 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 2022;
                                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) <= 5 ) )) ))  ) ) 
                                                                                                        { 
                                                                                                        __context__.SourceCodeLine = 2024;
                                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                        __context__.SourceCodeLine = 2025;
                                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                        } 
                                                                                                    
                                                                                                    } 
                                                                                                
                                                                                                } 
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        __context__.SourceCodeLine = 1971;
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 2032;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 17))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 2034;
                                                                        while ( Functions.TestForTrue  ( ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 2038;
                                                                            LVSTRING  .UpdateValue ( Functions.Left ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 1 ) )  ) ; 
                                                                            __context__.SourceCodeLine = 2039;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "P" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 2041;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( GLBL_DISPLAY_COMMANDS.COMMANDPOWERON , GLBL_DISPLAY.LASTCOMMAND ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 2043;
                                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                    __context__.SourceCodeLine = 2044;
                                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                    __context__.SourceCodeLine = 2045;
                                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 2047;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF , GLBL_DISPLAY.LASTCOMMAND ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 2049;
                                                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                        __context__.SourceCodeLine = 2050;
                                                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                        __context__.SourceCodeLine = 2051;
                                                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                                        } 
                                                                                    
                                                                                    }
                                                                                
                                                                                } 
                                                                            
                                                                            __context__.SourceCodeLine = 2034;
                                                                            } 
                                                                        
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 2057;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 19))  ) ) 
                                                                            { 
                                                                            } 
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        }
        
    object TCPCLIENT_OnSocketConnect_0 ( Object __Info__ )
    
        { 
        SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
            
            __context__.SourceCodeLine = 2067;
            GLBL_DISPLAY . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2068;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2069;
            CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SocketInfo__ ); }
        return this;
        
    }
    
object TCPCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 2074;
        GLBL_DISPLAY . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2075;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2076;
        CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 2080;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 2085;
        GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( GLBL_DISPLAY . CRXQUEUE + TCPCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 2086;
        Functions.ClearBuffer ( TCPCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 2087;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 1 ))  ) ) 
            {
            __context__.SourceCodeLine = 2088;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object DISPLAY_OBJ_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2093;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_OBJ  .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_OBJ  .UshortValue <= 5 ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2095;
            GLBL_DISPLAY . STATUSREADY = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2096;
            if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSPOWER ))  ) ) 
                { 
                __context__.SourceCodeLine = 2098;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOWERON) ; 
                __context__.SourceCodeLine = 2099;
                CreateWait ( "DISPPWR" , (DISPLAY_POWER_TIME  .UshortValue * 100) , DISPPWR_Callback ) ;
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2110;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), INPUT[ DISPLAY_OBJ  .UshortValue ]) ; 
                } 
            
            __context__.SourceCodeLine = 2112;
            GLBL_DISPLAY . STATUSREADY = (ushort) ( 1 ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 2114;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_OBJ  .UshortValue == 0) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_OBJ  .UshortValue == 99) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2116;
                GLBL_DISPLAY . STATUSREADY = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 2117;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF) ; 
                __context__.SourceCodeLine = 2118;
                GLBL_DISPLAY . STATUSREADY = (ushort) ( 1 ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void DISPPWR_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2101;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), INPUT[ DISPLAY_OBJ  .UshortValue ]) ; 
            __context__.SourceCodeLine = 2102;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_27__" , 100 , __SPLS_TMPVAR__WAITLABEL_27___Callback ) ;
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_27___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2104;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), ASPECT[ 1 ]) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_TYPE_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2124;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_TYPE  .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_TYPE  .UshortValue <= 19 ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2126;
            RUNINITIALIZATION (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_VOLUME_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2132;
        if ( Functions.TestForTrue  ( ( Functions.Not( DISPLAY_VOLUME_MUTE_OVERRIDE  .Value ))  ) ) 
            { 
            __context__.SourceCodeLine = 2134;
            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2135;
            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2136;
            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
            } 
        
        __context__.SourceCodeLine = 2138;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( DISPLAY_VOLUME  .UshortValue <= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ))  ) ) 
            {
            __context__.SourceCodeLine = 2139;
            GLBL_DISPLAY . STATUSVOLUME = (short) ( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 2140;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( DISPLAY_VOLUME  .UshortValue >= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ))  ) ) 
                {
                __context__.SourceCodeLine = 2141;
                GLBL_DISPLAY . STATUSVOLUME = (short) ( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) ; 
                }
            
            }
        
        __context__.SourceCodeLine = 2142;
        DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
        __context__.SourceCodeLine = 2143;
        SETVOLUME (  __context__ , (short)( GLBL_DISPLAY.STATUSVOLUME ), (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_VOLUME_UP_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2148;
        while ( Functions.TestForTrue  ( ( DISPLAY_VOLUME_UP  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 2150;
            GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2151;
            CreateWait ( "VOLUP" , 20 , VOLUP_Callback ) ;
            __context__.SourceCodeLine = 2148;
            } 
        
        __context__.SourceCodeLine = 2161;
        if ( Functions.TestForTrue  ( ( Functions.Not( DISPLAY_VOLUME_UP  .Value ))  ) ) 
            { 
            __context__.SourceCodeLine = 2163;
            GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2164;
            CancelWait ( "VOLUP" ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLUP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2153;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GLBL_DISPLAY.STATUSVOLUME >= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ))  ) ) 
                {
                __context__.SourceCodeLine = 2154;
                GLBL_DISPLAY . STATUSVOLUME = (short) ( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 2156;
                GLBL_DISPLAY . STATUSVOLUME = (short) ( (GLBL_DISPLAY.STATUSVOLUME + 1) ) ; 
                }
            
            __context__.SourceCodeLine = 2157;
            DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
            __context__.SourceCodeLine = 2158;
            SETVOLUME (  __context__ , (short)( GLBL_DISPLAY.STATUSVOLUME ), (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_VOLUME_UP_OnRelease_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2169;
        if ( Functions.TestForTrue  ( ( Functions.Not( DISPLAY_VOLUME_MUTE_OVERRIDE  .Value ))  ) ) 
            { 
            __context__.SourceCodeLine = 2171;
            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2172;
            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2173;
            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
            } 
        
        __context__.SourceCodeLine = 2175;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GLBL_DISPLAY.STATUSVOLUME >= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ))  ) ) 
            {
            __context__.SourceCodeLine = 2176;
            GLBL_DISPLAY . STATUSVOLUME = (short) ( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 2178;
            GLBL_DISPLAY . STATUSVOLUME = (short) ( (GLBL_DISPLAY.STATUSVOLUME + 1) ) ; 
            }
        
        __context__.SourceCodeLine = 2179;
        DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
        __context__.SourceCodeLine = 2180;
        SETVOLUME (  __context__ , (short)( GLBL_DISPLAY.STATUSVOLUME ), (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
        __context__.SourceCodeLine = 2181;
        GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_VOLUME_DN_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2186;
        while ( Functions.TestForTrue  ( ( DISPLAY_VOLUME_DN  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 2188;
            GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2189;
            CreateWait ( "VOLDN" , 20 , VOLDN_Callback ) ;
            __context__.SourceCodeLine = 2186;
            } 
        
        __context__.SourceCodeLine = 2199;
        if ( Functions.TestForTrue  ( ( Functions.Not( DISPLAY_VOLUME_DN  .Value ))  ) ) 
            { 
            __context__.SourceCodeLine = 2201;
            GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2202;
            CancelWait ( "VOLDN" ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLDN_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2191;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GLBL_DISPLAY.STATUSVOLUME <= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ))  ) ) 
                {
                __context__.SourceCodeLine = 2192;
                GLBL_DISPLAY . STATUSVOLUME = (short) ( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 2194;
                GLBL_DISPLAY . STATUSVOLUME = (short) ( (GLBL_DISPLAY.STATUSVOLUME - 1) ) ; 
                }
            
            __context__.SourceCodeLine = 2195;
            DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
            __context__.SourceCodeLine = 2196;
            SETVOLUME (  __context__ , (short)( GLBL_DISPLAY.STATUSVOLUME ), (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_VOLUME_DN_OnRelease_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2207;
        if ( Functions.TestForTrue  ( ( Functions.Not( DISPLAY_VOLUME_MUTE_OVERRIDE  .Value ))  ) ) 
            { 
            __context__.SourceCodeLine = 2209;
            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2210;
            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2211;
            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
            } 
        
        __context__.SourceCodeLine = 2213;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GLBL_DISPLAY.STATUSVOLUME <= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ))  ) ) 
            {
            __context__.SourceCodeLine = 2214;
            GLBL_DISPLAY . STATUSVOLUME = (short) ( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 2216;
            GLBL_DISPLAY . STATUSVOLUME = (short) ( (GLBL_DISPLAY.STATUSVOLUME - 1) ) ; 
            }
        
        __context__.SourceCodeLine = 2217;
        DISPLAY_VOLUME_FB  .Value = (ushort) ( GLBL_DISPLAY.STATUSVOLUME ) ; 
        __context__.SourceCodeLine = 2218;
        SETVOLUME (  __context__ , (short)( GLBL_DISPLAY.STATUSVOLUME ), (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
        __context__.SourceCodeLine = 2219;
        GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_VOLUME_MUTE_ON_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2224;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 3))  ) ) 
            { 
            __context__.SourceCodeLine = 2226;
            if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEMUTE ))  ) ) 
                {
                __context__.SourceCodeLine = 2227;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEON) ; 
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 2230;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEON) ; 
            }
        
        __context__.SourceCodeLine = 2231;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_28__" , 40 , __SPLS_TMPVAR__WAITLABEL_28___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_28___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 2232;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_VOLUME_MUTE_OFF_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2237;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 3))  ) ) 
            { 
            __context__.SourceCodeLine = 2239;
            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSVOLUMEMUTE)  ) ) 
                {
                __context__.SourceCodeLine = 2240;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEOFF) ; 
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 2243;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEOFF) ; 
            }
        
        __context__.SourceCodeLine = 2244;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_29__" , 40 , __SPLS_TMPVAR__WAITLABEL_29___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_29___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 2245;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_ASPECT_OnChange_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2250;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_ASPECT  .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_ASPECT  .UshortValue <= 5 ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 2251;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), ASPECT[ DISPLAY_ASPECT  .UshortValue ]) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SHARP_PROTOCOL_OnChange_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_ID__DOLLAR___OnChange_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2260;
        RUNINITIALIZATION (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DEBUG_OnChange_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2264;
        if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
            {
            __context__.SourceCodeLine = 2265;
            GLBL_DISPLAY . DEBUG = (ushort) ( 1 ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 2267;
            GLBL_DISPLAY . DEBUG = (ushort) ( 0 ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2271;
        GLBL_DISPLAY . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2275;
        GLBL_DISPLAY . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_POLL_OnPush_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        
        
        __context__.SourceCodeLine = 2281;
        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
            { 
            __context__.SourceCodeLine = 2283;
            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  ) ; 
            __context__.SourceCodeLine = 2284;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2286;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                __context__.SourceCodeLine = 2287;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2289;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 5) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 6) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 2290;
                    SETQUEUE (  __context__ , GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 2292;
                    SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 2293;
            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSPOWER)  ) ) 
                { 
                __context__.SourceCodeLine = 2295;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_30__" , 200 , __SPLS_TMPVAR__WAITLABEL_30___Callback ) ;
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_30___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            __context__.SourceCodeLine = 2298;
            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  ) ; 
            __context__.SourceCodeLine = 2299;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2301;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                __context__.SourceCodeLine = 2302;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2305;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLINPUT) ; 
                }
            
            __context__.SourceCodeLine = 2306;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_31__" , 200 , __SPLS_TMPVAR__WAITLABEL_31___Callback ) ;
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_31___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            __context__.SourceCodeLine = 2309;
            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  ) ; 
            __context__.SourceCodeLine = 2310;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2312;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                __context__.SourceCodeLine = 2313;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2316;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME) ; 
                }
            
            __context__.SourceCodeLine = 2317;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_32__" , 200 , __SPLS_TMPVAR__WAITLABEL_32___Callback ) ;
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_32___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            __context__.SourceCodeLine = 2320;
            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  ) ; 
            __context__.SourceCodeLine = 2321;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2323;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                __context__.SourceCodeLine = 2324;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2327;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE) ; 
                }
            
            __context__.SourceCodeLine = 2328;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_33__" , 200 , __SPLS_TMPVAR__WAITLABEL_33___Callback ) ;
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_33___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2330;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 11))  ) ) 
                { 
                __context__.SourceCodeLine = 2332;
                GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLLAMPHOURS  ) ; 
                __context__.SourceCodeLine = 2333;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLLAMPHOURS) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object IP_CONNECT_OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2345;
        if ( Functions.TestForTrue  ( ( IP_CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 2347;
            GLBL_DISPLAY . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2348;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 9))  ) ) 
                {
                __context__.SourceCodeLine = 2349;
                SETQUEUE (  __context__ , "ESC/VP.net\u0010\u0003\u0000\u0000\u0000\u0000") ; 
                }
            
            __context__.SourceCodeLine = 2350;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 10))  ) ) 
                { 
                __context__.SourceCodeLine = 2352;
                GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "00"  ) ; 
                __context__.SourceCodeLine = 2353;
                GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                } 
            
            __context__.SourceCodeLine = 2355;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 2359;
            GLBL_DISPLAY . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2360;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            __context__.SourceCodeLine = 2361;
            GLBL_DISPLAY . IPLOGIN = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2362;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 10))  ) ) 
                { 
                __context__.SourceCodeLine = 2364;
                GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u0002"  ) ; 
                __context__.SourceCodeLine = 2365;
                GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u0003"  ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX__DOLLAR___OnChange_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2372;
        GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( GLBL_DISPLAY . CRXQUEUE + RX__DOLLAR__  ) ; 
        __context__.SourceCodeLine = 2373;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 1 ))  ) ) 
            {
            __context__.SourceCodeLine = 2374;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2378;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    INPUT  = new CrestronString[ 6 ];
    for( uint i = 0; i < 6; i++ )
        INPUT [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
    ASPECT  = new CrestronString[ 6 ];
    for( uint i = 0; i < 6; i++ )
        ASPECT [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
    INPUTPOLLDATA  = new CrestronString[ 6 ];
    for( uint i = 0; i < 6; i++ )
        INPUTPOLLDATA [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
    TCPCLIENT  = new SplusTcpClient ( 2047, this );
    UDPCLIENT  = new SplusUdpSocket ( 2047, this );
    GLBL_DISPLAY  = new SLOCALDISPLAY( this, true );
    GLBL_DISPLAY .PopulateCustomAttributeList( false );
    GLBL_DISPLAY_COMMANDS  = new SDISPLAY( this, true );
    GLBL_DISPLAY_COMMANDS .PopulateCustomAttributeList( false );
    
    DISPLAY_VOLUME_MUTE_ON = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_MUTE_ON__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_MUTE_ON__DigitalInput__, DISPLAY_VOLUME_MUTE_ON );
    
    DISPLAY_VOLUME_MUTE_OFF = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_MUTE_OFF__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_MUTE_OFF__DigitalInput__, DISPLAY_VOLUME_MUTE_OFF );
    
    DISPLAY_VOLUME_MUTE_OVERRIDE = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_MUTE_OVERRIDE__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_MUTE_OVERRIDE__DigitalInput__, DISPLAY_VOLUME_MUTE_OVERRIDE );
    
    DISPLAY_VOLUME_UP = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_UP__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_UP__DigitalInput__, DISPLAY_VOLUME_UP );
    
    DISPLAY_VOLUME_DN = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_DN__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_DN__DigitalInput__, DISPLAY_VOLUME_DN );
    
    DISPLAY_POLL = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_POLL__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_POLL__DigitalInput__, DISPLAY_POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    IP_CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( IP_CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( IP_CONNECT__DigitalInput__, IP_CONNECT );
    
    DISPLAY_POWER_ON_FB = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAY_POWER_ON_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAY_POWER_ON_FB__DigitalOutput__, DISPLAY_POWER_ON_FB );
    
    DISPLAY_POWER_OFF_FB = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAY_POWER_OFF_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAY_POWER_OFF_FB__DigitalOutput__, DISPLAY_POWER_OFF_FB );
    
    DISPLAY_VOLUME_MUTE_ON_FB = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAY_VOLUME_MUTE_ON_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAY_VOLUME_MUTE_ON_FB__DigitalOutput__, DISPLAY_VOLUME_MUTE_ON_FB );
    
    DISPLAY_VOLUME_MUTE_OFF_FB = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAY_VOLUME_MUTE_OFF_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAY_VOLUME_MUTE_OFF_FB__DigitalOutput__, DISPLAY_VOLUME_MUTE_OFF_FB );
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    DISPLAY_ASPECT = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_ASPECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_ASPECT__AnalogSerialInput__, DISPLAY_ASPECT );
    
    DISPLAY_VOLUME = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_VOLUME__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_VOLUME__AnalogSerialInput__, DISPLAY_VOLUME );
    
    DISPLAY_POWER_TIME = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_POWER_TIME__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_POWER_TIME__AnalogSerialInput__, DISPLAY_POWER_TIME );
    
    DISPLAY_TYPE = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_TYPE__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_TYPE__AnalogSerialInput__, DISPLAY_TYPE );
    
    DISPLAY_OBJ = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_OBJ__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_OBJ__AnalogSerialInput__, DISPLAY_OBJ );
    
    SHARP_PROTOCOL = new Crestron.Logos.SplusObjects.AnalogInput( SHARP_PROTOCOL__AnalogSerialInput__, this );
    m_AnalogInputList.Add( SHARP_PROTOCOL__AnalogSerialInput__, SHARP_PROTOCOL );
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    DISPLAY_VOLUME_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAY_VOLUME_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAY_VOLUME_FB__AnalogSerialOutput__, DISPLAY_VOLUME_FB );
    
    DISPLAY_INPUT_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAY_INPUT_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAY_INPUT_FB__AnalogSerialOutput__, DISPLAY_INPUT_FB );
    
    DISPLAY_ASPECT_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAY_ASPECT_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAY_ASPECT_FB__AnalogSerialOutput__, DISPLAY_ASPECT_FB );
    
    DISPLAY_LAMP_HRS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAY_LAMP_HRS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAY_LAMP_HRS_FB__AnalogSerialOutput__, DISPLAY_LAMP_HRS_FB );
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    DISPLAY_ID__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( DISPLAY_ID__DOLLAR____AnalogSerialInput__, 7, this );
    m_StringInputList.Add( DISPLAY_ID__DOLLAR____AnalogSerialInput__, DISPLAY_ID__DOLLAR__ );
    
    RX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( RX__DOLLAR____AnalogSerialInput__, 255, this );
    m_StringInputList.Add( RX__DOLLAR____AnalogSerialInput__, RX__DOLLAR__ );
    
    LOGINNAME__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( LOGINNAME__DOLLAR____AnalogSerialInput__, 31, this );
    m_StringInputList.Add( LOGINNAME__DOLLAR____AnalogSerialInput__, LOGINNAME__DOLLAR__ );
    
    LOGINPASSWORD__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( LOGINPASSWORD__DOLLAR____AnalogSerialInput__, 31, this );
    m_StringInputList.Add( LOGINPASSWORD__DOLLAR____AnalogSerialInput__, LOGINPASSWORD__DOLLAR__ );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    GENERIC_POWERON = new Crestron.Logos.SplusObjects.StringInput( GENERIC_POWERON__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_POWERON__AnalogSerialInput__, GENERIC_POWERON );
    
    GENERIC_POWEROFF = new Crestron.Logos.SplusObjects.StringInput( GENERIC_POWEROFF__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_POWEROFF__AnalogSerialInput__, GENERIC_POWEROFF );
    
    GENERIC_MUTEON = new Crestron.Logos.SplusObjects.StringInput( GENERIC_MUTEON__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_MUTEON__AnalogSerialInput__, GENERIC_MUTEON );
    
    GENERIC_MUTEOFF = new Crestron.Logos.SplusObjects.StringInput( GENERIC_MUTEOFF__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_MUTEOFF__AnalogSerialInput__, GENERIC_MUTEOFF );
    
    GENERIC_VOLUMEUP = new Crestron.Logos.SplusObjects.StringInput( GENERIC_VOLUMEUP__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_VOLUMEUP__AnalogSerialInput__, GENERIC_VOLUMEUP );
    
    GENERIC_VOLUMEDOWN = new Crestron.Logos.SplusObjects.StringInput( GENERIC_VOLUMEDOWN__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_VOLUMEDOWN__AnalogSerialInput__, GENERIC_VOLUMEDOWN );
    
    GENERIC_HEADER = new Crestron.Logos.SplusObjects.StringInput( GENERIC_HEADER__AnalogSerialInput__, 7, this );
    m_StringInputList.Add( GENERIC_HEADER__AnalogSerialInput__, GENERIC_HEADER );
    
    GENERIC_FOOTER = new Crestron.Logos.SplusObjects.StringInput( GENERIC_FOOTER__AnalogSerialInput__, 7, this );
    m_StringInputList.Add( GENERIC_FOOTER__AnalogSerialInput__, GENERIC_FOOTER );
    
    GENERIC_INPUT = new InOutArray<StringInput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        GENERIC_INPUT[i+1] = new Crestron.Logos.SplusObjects.StringInput( GENERIC_INPUT__AnalogSerialInput__ + i, GENERIC_INPUT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( GENERIC_INPUT__AnalogSerialInput__ + i, GENERIC_INPUT[i+1] );
    }
    
    TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__DOLLAR____AnalogSerialOutput__, TX__DOLLAR__ );
    
    __SPLS_TMPVAR__WAITLABEL_26___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_26___CallbackFn );
    DISPPWR_Callback = new WaitFunction( DISPPWR_CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_27___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_27___CallbackFn );
    VOLUP_Callback = new WaitFunction( VOLUP_CallbackFn );
    VOLDN_Callback = new WaitFunction( VOLDN_CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_28___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_28___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_29___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_29___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_30___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_30___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_31___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_31___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_32___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_32___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_33___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_33___CallbackFn );
    
    TCPCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketConnect_0, false ) );
    TCPCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketDisconnect_1, false ) );
    TCPCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketStatus_2, false ) );
    TCPCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketReceive_3, false ) );
    DISPLAY_OBJ.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_OBJ_OnChange_4, false ) );
    DISPLAY_TYPE.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_TYPE_OnChange_5, false ) );
    DISPLAY_VOLUME.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_OnChange_6, false ) );
    DISPLAY_VOLUME_UP.OnDigitalChange.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_UP_OnChange_7, false ) );
    DISPLAY_VOLUME_UP.OnDigitalRelease.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_UP_OnRelease_8, false ) );
    DISPLAY_VOLUME_DN.OnDigitalChange.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_DN_OnChange_9, false ) );
    DISPLAY_VOLUME_DN.OnDigitalRelease.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_DN_OnRelease_10, false ) );
    DISPLAY_VOLUME_MUTE_ON.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_MUTE_ON_OnPush_11, false ) );
    DISPLAY_VOLUME_MUTE_OFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_MUTE_OFF_OnPush_12, false ) );
    DISPLAY_ASPECT.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_ASPECT_OnChange_13, false ) );
    SHARP_PROTOCOL.OnAnalogChange.Add( new InputChangeHandlerWrapper( SHARP_PROTOCOL_OnChange_14, false ) );
    DISPLAY_ID__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( DISPLAY_ID__DOLLAR___OnChange_15, false ) );
    DEBUG.OnDigitalChange.Add( new InputChangeHandlerWrapper( DEBUG_OnChange_16, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_17, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_18, false ) );
    DISPLAY_POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_POLL_OnPush_19, false ) );
    IP_CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( IP_CONNECT_OnChange_20, false ) );
    RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( RX__DOLLAR___OnChange_21, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_22, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DISPLAYS_V3_2_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_26___Callback;
private WaitFunction DISPPWR_Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_27___Callback;
private WaitFunction VOLUP_Callback;
private WaitFunction VOLDN_Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_28___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_29___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_30___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_31___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_32___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_33___Callback;


const uint DISPLAY_VOLUME_MUTE_ON__DigitalInput__ = 0;
const uint DISPLAY_VOLUME_MUTE_OFF__DigitalInput__ = 1;
const uint DISPLAY_VOLUME_MUTE_OVERRIDE__DigitalInput__ = 2;
const uint DISPLAY_VOLUME_UP__DigitalInput__ = 3;
const uint DISPLAY_VOLUME_DN__DigitalInput__ = 4;
const uint DISPLAY_POLL__DigitalInput__ = 5;
const uint DEBUG__DigitalInput__ = 6;
const uint IP_CONNECT__DigitalInput__ = 7;
const uint DISPLAY_ASPECT__AnalogSerialInput__ = 0;
const uint DISPLAY_VOLUME__AnalogSerialInput__ = 1;
const uint DISPLAY_POWER_TIME__AnalogSerialInput__ = 2;
const uint DISPLAY_TYPE__AnalogSerialInput__ = 3;
const uint DISPLAY_OBJ__AnalogSerialInput__ = 4;
const uint SHARP_PROTOCOL__AnalogSerialInput__ = 5;
const uint IP_PORT__AnalogSerialInput__ = 6;
const uint IP_ADDRESS__AnalogSerialInput__ = 7;
const uint DISPLAY_ID__DOLLAR____AnalogSerialInput__ = 8;
const uint RX__DOLLAR____AnalogSerialInput__ = 9;
const uint LOGINNAME__DOLLAR____AnalogSerialInput__ = 10;
const uint LOGINPASSWORD__DOLLAR____AnalogSerialInput__ = 11;
const uint MANUALCMD__AnalogSerialInput__ = 12;
const uint GENERIC_POWERON__AnalogSerialInput__ = 13;
const uint GENERIC_POWEROFF__AnalogSerialInput__ = 14;
const uint GENERIC_MUTEON__AnalogSerialInput__ = 15;
const uint GENERIC_MUTEOFF__AnalogSerialInput__ = 16;
const uint GENERIC_VOLUMEUP__AnalogSerialInput__ = 17;
const uint GENERIC_VOLUMEDOWN__AnalogSerialInput__ = 18;
const uint GENERIC_HEADER__AnalogSerialInput__ = 19;
const uint GENERIC_FOOTER__AnalogSerialInput__ = 20;
const uint GENERIC_INPUT__AnalogSerialInput__ = 21;
const uint DISPLAY_POWER_ON_FB__DigitalOutput__ = 0;
const uint DISPLAY_POWER_OFF_FB__DigitalOutput__ = 1;
const uint DISPLAY_VOLUME_MUTE_ON_FB__DigitalOutput__ = 2;
const uint DISPLAY_VOLUME_MUTE_OFF_FB__DigitalOutput__ = 3;
const uint CONNECT_FB__DigitalOutput__ = 4;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint DISPLAY_VOLUME_FB__AnalogSerialOutput__ = 1;
const uint DISPLAY_INPUT_FB__AnalogSerialOutput__ = 2;
const uint DISPLAY_ASPECT_FB__AnalogSerialOutput__ = 3;
const uint DISPLAY_LAMP_HRS_FB__AnalogSerialOutput__ = 4;
const uint TX__DOLLAR____AnalogSerialOutput__ = 5;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SDISPLAY : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public short  VOLUMEMIN = 0;
    
    [SplusStructAttribute(1, false, false)]
    public short  VOLUMEMAX = 0;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  COMMANDPOWERON;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  COMMANDPOWEROFF;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  COMMANDINPUT1;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  COMMANDINPUT2;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  COMMANDINPUT3;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  COMMANDINPUT4;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  COMMANDINPUT5;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  COMMANDASPECT1;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  COMMANDASPECT2;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  COMMANDASPECT3;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  COMMANDASPECT4;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  COMMANDASPECT5;
    
    [SplusStructAttribute(14, false, false)]
    public CrestronString  COMMANDVOLUMELEVEL;
    
    [SplusStructAttribute(15, false, false)]
    public CrestronString  COMMANDVOLUMEMUTEON;
    
    [SplusStructAttribute(16, false, false)]
    public CrestronString  COMMANDVOLUMEMUTEOFF;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  COMMANDSLEEP;
    
    [SplusStructAttribute(18, false, false)]
    public CrestronString  COMMANDPOLLPOWER;
    
    [SplusStructAttribute(19, false, false)]
    public CrestronString  COMMANDPOLLINPUT;
    
    [SplusStructAttribute(20, false, false)]
    public CrestronString  COMMANDPOLLVOLUME;
    
    [SplusStructAttribute(21, false, false)]
    public CrestronString  COMMANDPOLLMUTE;
    
    [SplusStructAttribute(22, false, false)]
    public CrestronString  COMMANDPOLLLAMPHOURS;
    
    [SplusStructAttribute(23, false, false)]
    public CrestronString  STX;
    
    [SplusStructAttribute(24, false, false)]
    public CrestronString  ETX;
    
    
    public SDISPLAY( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        COMMANDPOWERON  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDPOWEROFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDINPUT1  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDINPUT2  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDINPUT3  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDINPUT4  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDINPUT5  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDASPECT1  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDASPECT2  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDASPECT3  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDASPECT4  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDASPECT5  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDVOLUMELEVEL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDVOLUMEMUTEON  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDVOLUMEMUTEOFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDSLEEP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDPOLLPOWER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDPOLLINPUT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDPOLLVOLUME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDPOLLMUTE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDPOLLLAMPHOURS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class SLOCALDISPLAY : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSPOWER = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSINPUT = 0;
    
    [SplusStructAttribute(2, false, false)]
    public short  STATUSVOLUME = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  STATUSVOLUMEMUTE = 0;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  STATUSASPECT = 0;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  STATUSREADY = 0;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  STATUSVOLUMEHELD = 0;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  COMMANDCONFIRM = 0;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  COMMANDPOLL;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  COMMANDACK;
    
    [SplusStructAttribute(10, false, false)]
    public ushort  IPLOGIN = 0;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(12, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(13, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(14, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(15, false, false)]
    public CrestronString  CTXQUEUE;
    
    [SplusStructAttribute(16, false, false)]
    public CrestronString  CRXQUEUE;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  LASTCOMMAND;
    
    [SplusStructAttribute(18, false, false)]
    public ushort  NID = 0;
    
    [SplusStructAttribute(19, false, false)]
    public ushort  DEBUG = 0;
    
    
    public SLOCALDISPLAY( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        COMMANDPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        COMMANDACK  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        CTXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, Owner );
        CRXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, Owner );
        LASTCOMMAND  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, Owner );
        
        
    }
    
}

}
